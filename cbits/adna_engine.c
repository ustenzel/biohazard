#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

enum opcode { interp_exit
            , match_and_store_final
            , match_and_store_stack
            , add_stack_to_final
            , match_with_stack_or
            , match_with_stack_and
            , app_or
            , app_and
            , app_not
            , push_zeros
            , push_ones } ;


void run_interpreter( int64_t *pacc
                    , const uint8_t *paln
                    , uint8_t *pstack
                    , const uint8_t *code
                    , int rawlen )
{
    int len = (rawlen + 15) >> 4 ;
    while(1) {
        switch (*code++)
        {
            case interp_exit:
                return ;

            case match_and_store_final:
                {
                    uint8_t a = *code++ ;
                    uint8_t b = *code++ ;
                    uint8_t x = *code++ ;
                    uint8_t m = *code++ ;

                    int elen = (rawlen - a - b + 15) >> 4 ;
                    for( int i = 0 ; i != 16*elen ; ++i )
                    {
                        pacc[i+a] += (paln[i+b] & m) == x ;
                    }
                    break ;
                }

            case match_and_store_stack:
                {
                    uint8_t a = *code++ ;
                    uint8_t b = *code++ ;
                    uint8_t x = *code++ ;
                    uint8_t m = *code++ ;
                    pstack += 1024 ;

                    int elen = (rawlen - a + 15) >> 4 ;
                    for( int i = 0 ; i != a ; ++i )
                        pstack[i] = 0 ;

                    for( int i = 0 ; i != 16*elen ; ++i )
                    {
                        pstack[i+a] = (paln[i+b] & m) == x ;
                    }
                    break ;
                }

            case match_with_stack_and:
                {
                    uint8_t a = *code++ ;
                    uint8_t b = *code++ ;
                    uint8_t x = *code++ ;
                    uint8_t m = *code++ ;

                    int elen = (rawlen - a + 15) >> 4 ;
                    for( int i = 0 ; i != 16*elen ; ++i )
                    {
                        pstack[i+a] &= (paln[i+b] & m) == x ;
                    }
                    break ;
                }

            case match_with_stack_or:
                {
                    uint8_t a = *code++ ;
                    uint8_t b = *code++ ;
                    uint8_t x = *code++ ;
                    uint8_t m = *code++ ;

                    int elen = (rawlen - a - b + 15) >> 4 ;
                    for( int i = 0 ; i != 16*elen ; ++i )
                    {
                        pstack[i+a] |= (paln[i+b] & m) == x ;
                    }
                    break ;
                }

            case app_and:
                {
                    for( int i = 0 ; i != 16*len ; ++i )
                    {
                        pstack[i-1024] &= pstack[i] ;
                    }
                    pstack -= 1024 ;
                    break ;
                }

            case app_or:
                {
                    for( int i = 0 ; i != 16*len ; ++i )
                    {
                        pstack[i-1024] |= pstack[i] ;
                    }
                    pstack -= 1024 ;
                    break ;
                }

            case app_not:
                {
                    for( int i = 0 ; i != 16*len ; ++i )
                    {
                        pstack[i] = ~pstack[i] ;
                    }
                    break ;
                }

            case add_stack_to_final:
                {
                    for( int i = 0 ; i != 16*len ; ++i )
                    {
                        pacc[i] += pstack[i] ;
                    }
                    pstack -= 1024 ;
                    break ;
                }

            case push_zeros:
                {
                    pstack += 1024 ;
                    for( int i = 0 ; i != 16*len ; ++i )
                    {
                        pstack[i] = 0 ;
                    }
                    break ;
                }

            case push_ones:
                {
                    pstack += 1024 ;
                    for( int i = 0 ; i != rawlen ; ++i )
                    {
                        pstack[i] = 1 ;
                    }
                    break ;
                }

            default:
                fprintf( stderr, "illegal opcode %d \n", code[-1] ) ;
                exit(1) ;
        }
    }
}


