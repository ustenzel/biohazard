#include <sys/mman.h>

void my_munmap(void *len, unsigned char *p)
{
    munmap( p, (size_t)len ) ;
}

/* Moves l nybbles from *src to *dest.  Nybbles are packed two per byte,
 * high nybble first.  The 0th nybble is dropped and everything else
 * shifted appropriately.
 */
void move_nybbles( unsigned char *dest, unsigned char *src, size_t l )
{
    unsigned char a = *src++ ;
    for( ; l > 1 ; l -= 2 ) {
        unsigned char b = *src++ ;
        *dest++ = (a & 0xf) << 4 | (b & 0xf0) >> 4 ;
        a = b ;
    }
    if( l ) *dest = (a & 0xf) << 4 ;
}

