#include <stdint.h>

/*  let g = flip lookup $ zip [nucsA,nucsC,nucsG,nucsT] [0..]
    let f i = (\x y -> 4*x+y) <$> g (fst_np i) <*> g (snd_np i)
    map (maybe (-1) id . f . NPair) [0..255]                     */

static int8_t pairtab[256] =
    {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,0,4,-1,8,-1,-1,-1,12,-1,-1,-1,-1,-1,-1,-1,-1,1,5,-1,9,-1,-1,-1,13,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,2,6,-1,10,-1,-1,-1,14,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,3,7,-1,11,-1,-1,-1,15,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1};

void bump_basecompo( int64_t *acc_bc, const uint8_t *ref, int lref, int maxwidth, int width5, int width3 )
{
    for( int i = 0 ; i != width5 ; ++i )
        ++acc_bc[ ref[i] * maxwidth + i ] ;

    for( int i = -width3 ; i != 0 ; ++i )
        ++acc_bc[ (ref[i+lref]+6) * maxwidth + i ] ;
}

void bump_substitutions( int64_t* restrict acc_st, int64_t* restrict acc_cg, const uint8_t* restrict a_seq, int l_seq, int l5, int l3, int rng )
{
    // substitutions near 5' end
    for( int i = 0 ; i != l5 ; ++i )
    {
        int j = pairtab[ a_seq[i] ] ;
        if( j >= 0 )
            ++acc_st[ j * rng + i ] ;
    }

    // substitutions near 3' end
    for( int i = -l3 ; i != 0 ; ++i )
    {
        int j = pairtab[ a_seq[l_seq+i] ] ;
        if( j >= 0 )
            ++acc_st[ (17+j) * rng + i ] ;
    }

    // substitutions at CpG sites near 5' end
    for( int i = 0 ; i != l5 ; ++i )
    {
        if( (a_seq[i] & 0xf) == 2 && (a_seq[i+1] & 0xf) == 4 )
        {
            switch( a_seq[i] >> 8 ) {
                case 1: ++acc_cg[ 0 * rng + i ] ; break ;
                case 2: ++acc_cg[ 1 * rng + i ] ; break ;
                case 4: ++acc_cg[ 2 * rng + i ] ; break ;
                case 8: ++acc_cg[ 3 * rng + i ] ; break ;
            }
            switch( a_seq[i+1] >> 8 ) {
                case 1: ++acc_cg[ 4 * rng + i + 1 ] ; break ;
                case 2: ++acc_cg[ 5 * rng + i + 1 ] ; break ;
                case 3: ++acc_cg[ 6 * rng + i + 1 ] ; break ;
                case 4: ++acc_cg[ 7 * rng + i + 1 ] ; break ;
            }
        }
    }

    // substitutions at CpG sites near 3' end
    for( int i = -l3 ; i != 0 ; ++i )
    {
        if( (a_seq[l_seq+i] & 0xf) == 4 && (a_seq[l_seq+i-1] & 0xf) == 2 )
        {
            switch( a_seq[l_seq+i] >> 8 ) {
                case 1: ++acc_cg[ 13 * rng + i ] ; break ;
                case 2: ++acc_cg[ 14 * rng + i ] ; break ;
                case 4: ++acc_cg[ 15 * rng + i ] ; break ;
                case 8: ++acc_cg[ 16 * rng + i ] ; break ;
            }
            switch( a_seq[l_seq+i-1] >> 8 ) {
                case 1: ++acc_cg[  9 * rng +i - 1 ] ; break ;
                case 2: ++acc_cg[ 10 * rng +i - 1 ] ; break ;
                case 3: ++acc_cg[ 11 * rng +i - 1 ] ; break ;
                case 4: ++acc_cg[ 12 * rng +i - 1 ] ; break ;
            }
        }
    }
}

void revcom_ref( uint8_t* restrict dest, const uint8_t* restrict src, int len )
{
    for( int i = 0 ; i != len ; ++i )
        dest[len-i-1] = src[i] > 3 ? 255 : src[i] ^ 2 ;

}

void revcom_aln( uint8_t* restrict dest, const uint8_t* restrict src, int len )
{
    // rev-complement for nucleotide pairs
    for( int i = 0 ; i != len ; ++i )
    {
        uint8_t x = src[i] ;
        uint8_t y = ((x & 0x55) << 1) | ((x >> 1) & 0x55) ;
        uint8_t z = ((y & 0x33) << 2) | ((y >> 2) & 0x33) ;
        dest[len-i-1] = z ;
    }

    // padding at end
    for( int i = 0 ; i != 16 ; ++i )
        dest[len+i] = 0 ;
}
