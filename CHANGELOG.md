# 2.1 (2020-01-06)

 * Proper error handling.  Instead of calling error, now either a
   warning is logged or a proper exception is thrown.

 * Faster fastq parser, faster fusion of read pairs.

 * Cleanup of messy code.  Ghc >= 7.10 is now required.

# 2.0 (2019-05-28)

 * Switched from "iteratee" to "streaming".  The code looks much cleaner
   and is easier to understand.

 * Repaired mistreatment of bam headers when merging files.

# 1.1.0 (2018-10-15)

 * Fix for previous workaround.  Repairs writeBamFile.

# 1.1.0 (2018-10-12)

 * Work around changes in exceptions-0.10.0.  This version should
   compile cleanly and work with ghc-8.6.1.

# 1.0.4 (2018-09-02)

 * bugfix: memory leak in BGZF encoder

# 1.0.3 (2018-08-26)

 * bugfix: seeking had been broken for a while
 
# 1.0.2 (2018-07-08)

 * compatibility with ghc-8.4.1
 * bugfix: long headers were garbled when writing BAM files
 * more robust imports to avoid breakage when dependencies update
 
# 1.0.1 (2018-02-06)

 * Bugfix: merging of overlapping read pairs

# 1.0.0 (2018-01-28)

 * Removed dead code and failed experiments.  `biohazard` is now feature
   complete by decree.
