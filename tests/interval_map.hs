{-# Language TemplateHaskell #-}
module Main ( main ) where

import           Data.Bool
import qualified Data.IntervalMap as I
import           Data.List ( groupBy, sort )
import           System.Exit
import           Test.QuickCheck

newtype L = L { unL :: [(Word, Word, Char)] } deriving (Eq, Show)

instance Arbitrary L where
    arbitrary = L <$> sized go
      where
        go 0 = pure []
        go n = do x <- arbitrary
                  y <- arbitrary `suchThat` (\y -> y + x > x)
                  c <- chooseEnum ('a','z')
                  (:) (x,x+y,c) <$> go (n-1)


limited_sort :: L -> L
limited_sort = L . concat . map sort . groupBy (\(a,b,_) (c,d,_) -> (a,b) == (c,d)) . unL

(=:=) :: L -> L -> Bool
L xs =:= ys = L (sort xs) == limited_sort ys

-- Test if any sequence of inserts results in a valid structure.
prop_valid :: L -> Bool
prop_valid (L xs) = validate $ I.fromList xs
  where
    -- XXX  Should also check if common prefixes are indeed common.
    validate :: I.IntervalMap a -> Bool
    validate I.E = True
    validate  t  = valid_conses t && valid_left minBound t && valid_right minBound maxBound t

    valid_conses  I.E               =  False
    valid_conses (I.L   _ _ _ _)    =  True
    valid_conses (I.N _ _ _ l r)    =  valid_conses l && valid_conses r
    valid_conses (I.M _ _ _ _ l r)  =  no_more_N l && no_more_N r

    no_more_N  I.E               =  False
    no_more_N (I.L   _ _ _ _)    =  True
    no_more_N (I.N _ _ _ _ _)    =  False
    no_more_N (I.M _ _ _ _ l r)  =  no_more_N l && no_more_N r

    valid_left _  I.E               =  False
    valid_left m (I.L   a _ _ _)    =  a >= m
    valid_left m (I.N k _ _ l r)    =  k >= m && valid_left m l && valid_left k r
    valid_left _ (I.M _ a _ _ l r)  =  valid_left a l && valid_left a r

    valid_right _ _  I.E               =  False
    valid_right u v (I.L   _ b _ _)    =  u <= b && b <= v
    valid_right u v (I.N _ h _ l r)    =  u <= h && h <= v && valid_right u h l && valid_right u h r
    valid_right u v (I.M k _ h _ l r)  =  u <= k && k <= h && h <= v && valid_right u k l && valid_right k h r


-- Test if anything that went in comes out again.  Note that the
-- interval map sorts on coordinates, but not on the value.
prop_from_list :: L -> Bool
prop_from_list xs = xs =:= L (I.toList (I.fromList (unL xs)))

prop_lookup_overlap :: Word -> Word -> L -> Bool
prop_lookup_overlap a b (L xs) =
    L (filter (\(c,d,_) -> c < b && a < d) xs) =:=
    L (I.lookupOverlapWithKeys a b (I.fromList xs))

prop_lookup_contained :: Word -> Word -> L -> Bool
prop_lookup_contained a b (L xs) =
    L (filter (\(c,d,_) -> a <= c && d <= b) xs) =:=
    L (I.lookupContainedWithKeys a b (I.fromList xs))

prop_lookup_containing :: Word -> Word -> L -> Bool
prop_lookup_containing a b (L xs) =
    L (filter (\(c,d,_) -> c <= a && b <= d) xs) =:=
    L (I.lookupContainingWithKeys a b (I.fromList xs))

prop_lookup_partial :: Word -> Word -> L -> Bool
prop_lookup_partial a b (L xs) =
    L (filter (\(c,d,_) -> c < b && a < d && (a > c || b < d)) xs) =:=
    L (I.lookupPartialWithKeys a b (I.fromList xs))

prop_lookup_left :: Word -> L -> Bool
prop_lookup_left p (L xs) =
    case [ (a,b,c) | (a,b,c) <- xs, b <= p ] of
        [] -> I.lookupLeft p (I.fromList xs) == Nothing
        is -> let (v,u) = maximum [ (b,a) | (a,b,_) <- is ]
              in fmap (fmap sort) (I.lookupLeft p (I.fromList xs)) ==
                 Just (u,v, sort [ c | (a,b,c) <- is, (a,b)==(u,v) ])

prop_lookup_right :: Word -> L -> Bool
prop_lookup_right p (L xs) =
    case [ (a,b,c) | (a,b,c) <- xs, a >= p ] of
        [] -> I.lookupRight p (I.fromList xs) == Nothing
        is -> let (u,v,_) = minimum is
              in fmap (fmap sort) (I.lookupRight p (I.fromList xs)) ==
                 Just (u,v, sort [ c | (a,b,c) <- is, (a,b)==(u,v) ])

prop_delete :: Word -> Word -> Char -> L -> Bool
prop_delete a b c xs =
    xs =:= L (I.toList (I.deleteBy (==c) a b $ I.fromList ((a,b,c) : unL xs)))

prop_lookup:: Word -> Word -> L -> Bool
prop_lookup a b (L xs) =
    L (filter (\(c,d,_) -> a == c && b == d) xs) =:=
    L (I.lookupWithKeys a b (I.fromList xs))


-- bizarre splice to make Ghc/TH happy
$(return [])

main :: IO ()
main = $quickCheckAll >>= bool exitFailure exitSuccess

