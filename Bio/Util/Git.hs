{-# LANGUAGE CPP, TemplateHaskell #-}

-- | Utilities to embed Git version information at compile time, and the
-- embedded Git version of biohazard.
module Bio.Util.Git
    ( GitInfo
    , gitInfo
    , giBranch
    , giDescribe
    , giFullVersion

    , gitInfoBiohazard
    , versionBiohazard
    ) where

import Data.Version             ( Version )
import Language.Haskell.TH
import Paths_biohazard          ( version )
import Prelude

#if defined(MIN_VERSION_githash)

import GitHash

#if MIN_VERSION_template_haskell(2,17,0)
gitInfo :: Code Q (Either String GitInfo)
#else
gitInfo :: Q (TExp (Either String GitInfo))
#endif
gitInfo = tGitInfoCwdTry

gitInfoBiohazard :: Either String GitInfo
gitInfoBiohazard = $$(tGitInfoCwdTry)
{-# NOINLINE gitInfoBiohazard #-}

giFullVersion :: Either String GitInfo -> Maybe String
giFullVersion (Left   _) = Nothing
giFullVersion (Right gi)
    | null (giBranch gi) = Just $ giDescribe gi
    | otherwise          = Just $ giBranch gi ++ ":" ++ giDescribe gi

#else

data GitInfo = GitInfo

gitInfo :: Q (TExp (Either String GitInfo))
gitInfo = [|| gitInfoBiohazard ||]

gitInfoBiohazard :: Either String GitInfo
gitInfoBiohazard = Left "couldn't use githash package"
{-# NOINLINE gitInfoBiohazard #-}

giBranch, giDescribe :: GitInfo -> String
giBranch      _ = []
giDescribe    _ = []

giFullVersion :: Either String GitInfo -> Maybe String
giFullVersion (Left        _) = Nothing
giFullVersion (Right GitInfo) = Just []

#endif

versionBiohazard :: Version
versionBiohazard = version
{-# NOINLINE versionBiohazard #-}
