{-# LANGUAGE CPP #-}
module Bio.Util.Nub (
    nubHash, nubHashOn,
    nubInt,  nubIntOn,
    nubOrd,  nubOrdOn
    ) where

import Prelude
import Data.Hashable ( Hashable )
import qualified Data.HashSet as H

#if MIN_VERSION_containers(0,6,0)

import Data.Containers.ListUtils ( nubInt, nubIntOn, nubOrd, nubOrdOn )

#else

import qualified Data.IntSet as I
import qualified Data.Set    as S

nubInt :: [Int] -> [Int]
nubInt = nubIntOn id
{-# INLINE nubInt #-}

nubIntOn :: (a -> Int) -> [a] -> [a]
nubIntOn f = go I.empty
  where
    go _ [] = []
    go h (x:xs) | f x `I.member` h = go h xs
                | otherwise        = x : go (I.insert (f x) h) xs
{-# INLINE nubIntOn #-}

nubOrd :: Ord a => [a] -> [a]
nubOrd = nubOrdOn id
{-# INLINE nubOrd #-}

nubOrdOn :: Ord b => (a -> b) -> [a] -> [a]
nubOrdOn f = go S.empty
  where
    go _ [] = []
    go h (x:xs) | f x `S.member` h = go h xs
                | otherwise        = x : go (S.insert (f x) h) xs
{-# INLINE nubOrdOn #-}

#endif

nubHash :: Hashable a => [a] -> [a]
nubHash = nubHashOn id
{-# INLINE nubHash #-}

nubHashOn :: Hashable b => (a -> b) -> [a] -> [a]
nubHashOn f = go H.empty
  where
    go _ [] = []
    go h (x:xs) | f x `H.member` h = go h xs
                | otherwise        = x : go (H.insert (f x) h) xs
{-# INLINE nubHashOn #-}

