{-# LANGUAGE CPP #-}
-- | Utilities to read multibyte quantities from arbitrary positions.
module Bio.Util.Storable
    ( peekWord8
    , peekUnalnWord16LE
    , peekUnalnWord16BE
    , peekUnalnWord32LE
    , peekUnalnWord32BE
    , peekUnalnWord64LE
    , peekUnalnWord64BE
    , pokeUnalnWord16LE
    , pokeUnalnWord32LE
    , pokeUnalnWord64LE
    ) where

#if __GLASGOW_HASKELL__ >= 710
#define HAVE_BYTESWAP_PRIMOPS
#endif

#if defined(i386_HOST_ARCH) || defined(x86_64_HOST_ARCH)
#define MEM_UNALIGNED_OPS
#endif

import Data.Word
import Foreign.Ptr
import Foreign.Storable
import Prelude

peekWord8 :: Ptr a -> IO Word8
peekWord8 = peek . castPtr

#if defined(MEM_UNALIGNED_OPS) && defined(WORDS_BIGENDIAN) && defined(HAVE_BYTESWAP_PRIMOPS)
peekUnalnWord16LE :: Ptr a -> IO Word16
peekUnalnWord16LE = fmap byteSwap16 . peek . castPtr

peekUnalnWord32LE :: Ptr a -> IO Word32
peekUnalnWord32LE = fmap byteSwap32 . peek . castPtr

peekUnalnWord64LE :: Ptr a -> IO Word64
peekUnalnWord64LE = fmap byteSwap64 . peek . castPtr

pokeUnalnWord16LE :: Ptr a -> Word16 -> IO ()
pokeUnalnWord16LE p w = poke (castPtr p) (byteSwap16 w)

pokeUnalnWord32LE :: Ptr a -> Word32 -> IO ()
pokeUnalnWord32LE p w = poke (castPtr p) (byteSwap32 w)

pokeUnalnWord64LE :: Ptr a -> Word64 -> IO ()
pokeUnalnWord64LE p w = poke (castPtr p) (byteSwap64 w)

#elif defined(MEM_UNALIGNED_OPS) && !defined(WORDS_BIGENDIAN)
peekUnalnWord16LE :: Ptr a -> IO Word16
peekUnalnWord16LE = peek . castPtr

peekUnalnWord32LE :: Ptr a -> IO Word32
peekUnalnWord32LE = peek . castPtr

peekUnalnWord64LE :: Ptr a -> IO Word64
peekUnalnWord64LE = peek . castPtr

pokeUnalnWord16LE :: Ptr a -> Word16 -> IO ()
pokeUnalnWord16LE p w = poke (castPtr p) w

pokeUnalnWord32LE :: Ptr a -> Word32 -> IO ()
pokeUnalnWord32LE p w = poke (castPtr p) w

pokeUnalnWord64LE :: Ptr a -> Word64 -> IO ()
pokeUnalnWord64LE p w = poke (castPtr p) w

#else
peekUnalnWord16LE :: Ptr a -> IO Word16
peekUnalnWord16LE p = do
    x <- fromIntegral <$> peekWord8 (plusPtr p 0)
    y <- fromIntegral <$> peekWord8 (plusPtr p 1)
    return $! x .|. unsafeShiftL y 8

peekUnalnWord32LE :: Ptr a -> IO Word32
peekUnalnWord32LE p = do
    x <- fromIntegral <$> peekWord8 (plusPtr p 0)
    y <- fromIntegral <$> peekWord8 (plusPtr p 1)
    z <- fromIntegral <$> peekWord8 (plusPtr p 2)
    w <- fromIntegral <$> peekWord8 (plusPtr p 3)
    return $! x .|. unsafeShiftL y 8 .|. unsafeShiftL z 16 .|. unsafeShiftL w 24

peekUnalnWord64LE :: Ptr a -> IO Word64
peekUnalnWord64LE p = do
    x <- fromIntegral <$> peekUnalnWord32LE (plusPtr p 0)
    y <- fromIntegral <$> peekUnalnWord32LE (plusPtr p 4)
    return $! x .|. unsafeShiftL y 32

pokeUnalnWord16LE :: Ptr a -> Word16 -> IO ()
pokeUnalnWord16LE p w = do pokeByteOff p 0 (fromIntegral $ shiftR w  0 :: Word8)
                           pokeByteOff p 1 (fromIntegral $ shiftR w  8 :: Word8)

pokeUnalnWord32LE :: Ptr a -> Word32 -> IO ()
pokeUnalnWord32LE p w = do pokeByteOff p 0 (fromIntegral $ shiftR w  0 :: Word8)
                           pokeByteOff p 1 (fromIntegral $ shiftR w  8 :: Word8)
                           pokeByteOff p 2 (fromIntegral $ shiftR w 16 :: Word8)
                           pokeByteOff p 3 (fromIntegral $ shiftR w 24 :: Word8)

pokeUnalnWord64LE :: Ptr a -> Word64 -> IO ()
pokeUnalnWord64LE p w = do pokeByteOff p 0 (fromIntegral $ shiftR w  0 :: Word8)
                           pokeByteOff p 1 (fromIntegral $ shiftR w  8 :: Word8)
                           pokeByteOff p 2 (fromIntegral $ shiftR w 16 :: Word8)
                           pokeByteOff p 3 (fromIntegral $ shiftR w 24 :: Word8)
                           pokeByteOff p 4 (fromIntegral $ shiftR w 32 :: Word8)
                           pokeByteOff p 5 (fromIntegral $ shiftR w 40 :: Word8)
                           pokeByteOff p 6 (fromIntegral $ shiftR w 48 :: Word8)
                           pokeByteOff p 7 (fromIntegral $ shiftR w 56 :: Word8)
#endif


#if defined(MEM_UNALIGNED_OPS) && !defined(WORDS_BIGENDIAN) && defined(HAVE_BYTESWAP_PRIMOPS)
peekUnalnWord16BE :: Ptr a -> IO Word16
peekUnalnWord16BE = fmap byteSwap16 . peek . castPtr

peekUnalnWord32BE :: Ptr a -> IO Word32
peekUnalnWord32BE = fmap byteSwap32 . peek . castPtr

peekUnalnWord64BE :: Ptr a -> IO Word64
peekUnalnWord64BE = fmap byteSwap64 . peek . castPtr

#elif defined(MEM_UNALIGNED_OPS) && defined(WORDS_BIGENDIAN)
peekUnalnWord16BE :: Ptr a -> IO Word16
peekUnalnWord16BE = peek . castPtr

peekUnalnWord32BE :: Ptr a -> IO Word32
peekUnalnWord32BE = peek . castPtr

peekUnalnWord64BE :: Ptr a -> IO Word64
peekUnalnWord64BE = peek . castPtr

#else
peekUnalnWord16BE :: Ptr a -> IO Word16
peekUnalnWord16BE p = do
    x <- fromIntegral <$> peekWord8 (plusPtr p 0)
    y <- fromIntegral <$> peekWord8 (plusPtr p 1)
    return $! y .|. unsafeShiftL x 8

peekUnalnWord32BE :: Ptr a -> IO Word32
peekUnalnWord32BE p = do
    x <- fromIntegral <$> peekWord8 (plusPtr p 0)
    y <- fromIntegral <$> peekWord8 (plusPtr p 1)
    z <- fromIntegral <$> peekWord8 (plusPtr p 2)
    w <- fromIntegral <$> peekWord8 (plusPtr p 3)
    return $! w .|. unsafeShiftL z 8 .|. unsafeShiftL y 16 .|. unsafeShiftL x 24

peekUnalnWord64BE :: Ptr a -> IO Word64
peekUnalnWord64BE p = do
    x <- fromIntegral <$> peekUnalnWord32BE (plusPtr p 0)
    y <- fromIntegral <$> peekUnalnWord32BE (plusPtr p 4)
    return $! y .|. unsafeShiftL x 32
#endif

{-# INLINE peekWord8 #-}
{-# INLINE peekUnalnWord16LE #-}
{-# INLINE peekUnalnWord16BE #-}
{-# INLINE peekUnalnWord32LE #-}
{-# INLINE peekUnalnWord32BE #-}
{-# INLINE peekUnalnWord64LE #-}
{-# INLINE peekUnalnWord64BE #-}
{-# INLINE pokeUnalnWord16LE #-}
{-# INLINE pokeUnalnWord32LE #-}
{-# INLINE pokeUnalnWord64LE #-}
