{-# LANGUAGE CPP #-}
module Bio.Util.MMap ( createMmapFile, mmapFile, mmapWithFilePtr ) where

import Control.Monad.Catch      ( bracket, MonadMask(..) )
import Control.Monad.IO.Class   ( MonadIO(..) )
import Foreign.C.Error          ( getErrno, errnoToIOError )
import Foreign.C.Types
import Foreign.ForeignPtr
import Foreign.Ptr
import Prelude
import System.Posix.Files       ( fileSize, getFdStatus, setFdSize )
import System.Posix.IO          ( openFd, closeFd, defaultFileFlags, OpenMode(..), OpenFileFlags(..) )
import System.Posix.Types       ( COff(..), Fd(..), FileMode )

#include <sys/mman.h>

myOpenFd :: FilePath -> OpenMode -> Maybe FileMode -> OpenFileFlags -> IO Fd
#if MIN_VERSION_unix(2,8,0)
myOpenFd fp md cr ff = openFd fp md ff { creat = cr }
#else
myOpenFd = openFd
#endif

-- | Maps a whole file into memory, returns the size in bytes and a
-- 'ForeignPtr' to the contents.
mmapFile :: FilePath -> IO (Int, ForeignPtr a)
mmapFile fp =
    bracket (myOpenFd fp ReadOnly Nothing defaultFileFlags) closeFd $ \fd -> do
        size <- fileSize <$> getFdStatus fd
        if size <= 0
            then (,) 0 <$> newForeignPtr_ nullPtr
            else do
                ptr <- mmap nullPtr (fromIntegral size) (#const PROT_READ) (#const MAP_SHARED) fd 0
                if ptrToIntPtr ptr == #const MAP_FAILED
                    then do errno <- getErrno
                            ioError $ errnoToIOError "mmapFile" errno Nothing (Just fp)
                    else (,) (fromIntegral size) <$> newForeignPtrEnv my_munmap (intPtrToPtr $ fromIntegral size) ptr

-- | Maps a whole file into memory, returns the size in bytes and a
-- 'ForeignPtr' to the contents.
mmapWithFilePtr :: (MonadIO m, MonadMask m) => FilePath -> (Ptr a -> Int -> m b) -> m b
mmapWithFilePtr fp k =
    bracket (liftIO $ myOpenFd fp ReadOnly Nothing defaultFileFlags) (liftIO . closeFd) $ \fd -> do
        size <- liftIO $ fileSize <$> getFdStatus fd
        if size <= 0
            then k nullPtr 0
            else bracket (do p <- liftIO $ mmap nullPtr (fromIntegral size) (#const PROT_READ) (#const MAP_SHARED) fd 0
                             if ptrToIntPtr p == #const MAP_FAILED
                                then liftIO $ do errno <- getErrno
                                                 ioError $ errnoToIOError "mmapFile" errno Nothing (Just fp)
                                else pure p)
                         (liftIO . (`munmap` fromIntegral size))
                         (`k` fromIntegral size)

-- | Creates a new file of a desired initial size, filled with all
-- zeros, maps it into memory, and calls a function to fill it.  That
-- function returns a pointer to the first unused byte in the file, and
-- it is then truncated accordingly.
createMmapFile :: FilePath -> CSize -> (Ptr a -> IO (Ptr a, b)) -> IO b
createMmapFile fp sz k =
    let flags = defaultFileFlags { noctty = True, trunc = True } in
    bracket (myOpenFd fp ReadWrite (Just 0x1b6) flags) closeFd $ \fd -> do
        setFdSize fd (fromIntegral sz)
        bracket (mmap nullPtr sz (#const PROT_READ | PROT_WRITE) (#const MAP_SHARED) fd 0)
                (flip munmap sz) $ \p -> do
            (p',r) <- k p
            setFdSize fd (fromIntegral $ minusPtr p' p)
            return r

foreign import ccall unsafe "&my_munmap"        my_munmap :: FunPtr (Ptr () -> Ptr a -> IO ())
foreign import ccall unsafe "sys/mman.h mmap"   mmap      :: Ptr a -> CSize -> CInt -> CInt -> Fd -> COff -> IO (Ptr a)
foreign import ccall unsafe "sys/mman.h munmap" munmap    :: Ptr a -> CSize -> IO ()

