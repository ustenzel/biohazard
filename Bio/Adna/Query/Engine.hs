module Bio.Adna.Query.Engine ( evalVFun ) where

import Bio.Adna                                 ( Alignment(..), FragType(..), NPair )
import Bio.Adna.Query.Language
import Bio.Prelude
import Bio.Streaming
import Foreign.C.Types                          ( CInt(..) )

import qualified Data.Vector.Storable           as V
import qualified Data.Vector.Storable.Mutable   as VM
import qualified Streaming.Prelude              as Q

-- We want a fast interpreter.  Main ideas over the old naive one:
--
-- * Accumulate into a mutable buffer instead of zipping vectors.
--   * Use a big buffer (1024) and don't bother with reallocation
--   * Limit the (effective) length of alignments to 1000.   Nobody will
--   ever notice.
--
-- * Round up to multiples of 16 to allow the use of SSE.  With the
--   alignment padded (done in Bio.Adna), we can load 16 bytes at a time
--   and process them in parallel.
--
-- * Use a byte code interpreter written in C
--   * Gcc vectorizes the loops.
--
-- This works!  Interpretation overhead went down by an order of magnitude!


-- | Substitutes a different expression for the 'TrimmedP' predicate,
-- and simplifies the result.
withTrimmed :: Pred a -> Pred a -> Pred a
withTrimmed _  (Match a b x m)  =  Match a b x m
withTrimmed tr (p :&&: q)       =  withTrimmed tr p .&&. withTrimmed tr q
withTrimmed tr (p :||: q)       =  withTrimmed tr p .||. withTrimmed tr q
withTrimmed tr (NotP   p)       =  notP $ withTrimmed tr p
withTrimmed _   FalseP          =  FalseP
withTrimmed _   TrueP           =  TrueP
withTrimmed tr  TrimmedP        =  tr


-- | Compiles a predicate to bytecode.
compile :: Pred NPair -> V.Vector Word8
compile = V.fromList . ($ [ op Interp_exit ]) . toplevel
  where
    toplevel (Match a b x m) = (++) [ op Match_and_store_final, a, b, x, m ]
    toplevel  FalseP         = id
    toplevel  p              = to_stack p . (:) (op Add_stack_to_final)

    to_stack (Match a b x m) = (++) [ op Match_and_store_stack, a, b, x, m ]
    to_stack (p :||: q)      = to_stack p . with_stack_or q
    to_stack (p :&&: q)      = to_stack p . with_stack_and q
    to_stack (NotP p)        = to_stack p . (:) (op App_not)
    to_stack  FalseP         = (:) (op Push_zeros)
    to_stack  TrueP          = (:) (op Push_ones)
    to_stack  TrimmedP       = error "can't happen"

    with_stack_or (Match a b x m) = (++) [ op Match_with_stack_or, a, b, x, m ]
    with_stack_or p = to_stack p . (:) (op App_or)

    with_stack_and (Match a b x m) = (++) [ op Match_with_stack_and, a, b, x, m ]
    with_stack_and p = to_stack p . (:) (op App_and)

    op ::  OpCode -> Word8
    op = fromIntegral . fromEnum


-- constants from query.c
data OpCode = Interp_exit
            | Match_and_store_final
            | Match_and_store_stack
            | Add_stack_to_final
            | Match_with_stack_or
            | Match_with_stack_and
            | App_or
            | App_and
            | App_not
            | Push_zeros
            | Push_ones
  deriving ( Show, Eq, Enum )

-- | Computes the amount of stack needed by a byte code program.
stackDepth :: V.Vector Word8 -> Int
stackDepth = maximum . scanl (+) 0 . use_of . V.toList
  where
    use_of :: [Word8] -> [Int]
    use_of [    ] = []
    use_of (x:xs) = case toEnum $ fromIntegral x of
        Interp_exit           ->      use_of xs
        Match_and_store_final ->      use_of (drop 4 xs)
        Match_and_store_stack ->  1 : use_of (drop 4 xs)
        Add_stack_to_final    -> -1 : use_of xs
        Match_with_stack_or   ->      use_of (drop 4 xs)
        Match_with_stack_and  ->      use_of (drop 4 xs)
        App_or                -> -1 : use_of xs
        App_and               -> -1 : use_of xs
        App_not               ->      use_of xs
        Push_zeros            ->  1 : use_of xs
        Push_ones             ->  1 : use_of xs


evalVFun :: MonadIO m => VFun b -> Stream (Of Alignment) m r -> m (Of b r)
evalVFun (VFun g fs) s = do
    stack <- liftIO $ VM.new (1024*max_depth)
    accs <- liftIO $ replicateM (length fs) (VM.replicate 1024 0)

    r <- Q.mapM_ (\aln -> liftIO $ VM.unsafeWith stack $ \pstack ->
                                   V.unsafeWith (a_sequence aln) $ \paln ->
                                   let laln = min (div (V.length (a_sequence aln) + 15) 16) 1000
                                       bcodes = case a_fragment_type aln of Complete -> bcodes_t ; _ -> bcodes_f
                                   in zipWithM_ (app_one_fun pstack laln paln) bcodes accs) s

    liftIO $ (:> r) . g . zipWith (\f v -> mapStylish (const v) f) fs <$> mapM V.unsafeFreeze accs
  where
    bcodes_f  = map (compile . withTrimmed FalseP . s_payload) fs
    bcodes_t  = map (compile . withTrimmed TrueP  . s_payload) fs
    max_depth = maximum $ map stackDepth $ mempty : bcodes_f ++ bcodes_t

    app_one_fun pstack laln paln bcode acc =
        VM.unsafeWith acc $ \pacc ->
        V.unsafeWith bcode $ \pcode ->
        run_interpreter pacc paln pstack pcode (fromIntegral laln)


foreign import ccall unsafe "run_interpreter"
    run_interpreter :: Ptr Int              -- array of accumulated counters
                    -> Ptr NPair            -- alignment array
                    -> Ptr Word8            -- stack array
                    -> Ptr Word8            -- byte code
                    -> CInt                 -- length of alignment
                    -> IO ()

