module Bio.Adna.Query.Language
    ( module Bio.Adna.Query.Language ) where

import Bio.Adna
import Bio.Prelude hiding ( isBase, isGap )

import qualified Data.Vector.Storable as V

-- ^ An embedded language of substitution pattern charts.
-- We split the language into predicates to be evaluated on each base
-- and functions to be evaluated on vectors of counters.  The former is
-- embedded deeply (see 'Pred'), the latter embedding is shallow (see
-- 'VFun').  This way, we can compile the inner loop to byte code, while
-- retaining flexibility for the uncritical high level operations.

-- | Our 'LineStyle' gets mapped to Cairo line styles.  If we're
-- compiled without charting support, it is just dragged along.
data LineStyle = Solid Double Double Double
               | Dashed [Double] Double Double Double
               | Hidden
  deriving Show

red, green, blue, black :: (Double,Double,Double)
red   = (,,)  1    0    0
green = (,,)  0    1    0
blue  = (,,)  0.25 0.25 1
black = (,,)  0    0    0

uncurry3 :: (a->b->c->r) -> (a,b,c) -> r
uncurry3 f (a,b,c) = f a b c

hidden :: Double -> Double -> Double -> LineStyle
hidden _ _ _ = Hidden


data Stylish f a = Stylish
    { s_label     :: String
    , s_linestyle :: Maybe (Double -> Double -> Double -> LineStyle)
    , s_linecolor :: Maybe (Double,Double,Double)
    , s_payload   :: f a }

instance Functor f => Functor (Stylish f) where
    fmap f (Stylish l s c p) = Stylish l s c (fmap f p)

instance Applicative f => Applicative (Stylish f) where
    pure = Stylish "" Nothing Nothing . pure
    Stylish l1 s1 c1 p1 <*> Stylish l2 s2 c2 p2 =
        Stylish (l1++l2) (mplus s1 s2) (mplus c1 c2) (p1 <*> p2)

mapStylish :: (f a -> g b) -> Stylish f a -> Stylish g b
mapStylish f (Stylish l s c x) = Stylish l s c (f x)

unstyled :: f a -> Stylish f a
unstyled = Stylish "" Nothing Nothing

styled :: (Double -> Double -> Double -> LineStyle) -> Stylish f a -> Stylish f a
styled s f = f { s_linestyle = Just s }

colored :: (Double,Double,Double) -> Stylish f a -> Stylish f a
colored rgb f = f { s_linecolor = Just (rgb) }

lbl :: String -> Stylish f a -> Stylish f a
lbl l s = s { s_label = l }


-- | A @Pred a@ will be interpreted as a predicate over @a@.  Predicates
-- are evaluated at every site of each read, and accumulated for each
-- position in the reads.

data Pred a = Match { _load_offset  :: !Word8
                    , _store_offset :: !Word8
                    , _template     :: !Word8
                    , _mask         :: !Word8 }
            | Pred a :&&: Pred a
            | Pred a :||: Pred a
            | NotP (Pred a)
            | FalseP
            | TrueP
            | TrimmedP              -- ^ True if the fragment had its adapter trimmed.
  deriving Show

mapMatch :: (Word8 -> Word8 -> Word8 -> Word8 -> (Word8, Word8, Word8, Word8)) -> Pred a -> Pred b
mapMatch f (Match a b x m)  =  Match a' b' x' m' where (a',b',x',m') = f a b x m
mapMatch f (p :&&: q)       =  mapMatch f p :&&: mapMatch f q
mapMatch f (p :||: q)       =  mapMatch f p :||: mapMatch f q
mapMatch f (NotP   p)       =  NotP $ mapMatch f p
mapMatch _  FalseP          =  FalseP
mapMatch _  TrueP           =  TrueP
mapMatch _  TrimmedP        =  TrimmedP


-- | Smart constructor for ':&&:'.  Absorbs into 'FalseP', absorbs
-- 'TrueP', fuses conjunctions of two 'Match'es.  'Match' commutes to
-- the left, to make fusion more likely, and associates to the left, for
-- less stack usage.
(.&&.) :: Pred a -> Pred a -> Pred a
FalseP .&&. _          =  FalseP
_      .&&. FalseP     =  FalseP
TrueP  .&&. q          =  q
q      .&&. TrueP      =  q
Match a1 b1 x1 m1 .&&. Match a2 b2 x2 m2
    | a1 == a2 && b1 == b2 && x1 .&. m1 .&. m2 == x2 .&. m1 .&. m2  =  Match a1 b1 (x1 .|. x2) (m1 .|. m2)
    | a1 == a2 && b1 == b2                                          =  FalseP
p      .&&. q@Match{}  =  q :&&: p
p      .&&. (q :&&: r) =  (p .&&. q) :&&: r
p      .&&. q          =  p :&&: q

-- | Smart constructor for ':||:'. Absorbs 'FalseP', absorbs into
-- 'TrueP'.  Associates to the left, for less stack usage.
(.||.) :: Pred a -> Pred a -> Pred a
FalseP .||. q           =  q
p      .||. FalseP      =  p
TrueP  .||. _           =  TrueP
_      .||. TrueP       =  TrueP
p      .||. (q :||: r)  =  (p :||: q) :||: r
p      .||. q           =  p :||: q

-- | Smart constructor for 'NotP'.  Simplifies constants.
notP :: Pred a -> Pred a
notP TrueP  = FalseP
notP FalseP = TrueP
notP p      = NotP p


isA, isC, isG, isT, isN, isGap :: Pred Nucleotides
isA   = Match 0 0 (unNs nucsA) 0xf
isC   = Match 0 0 (unNs nucsC) 0xf
isG   = Match 0 0 (unNs nucsG) 0xf
isT   = Match 0 0 (unNs nucsT) 0xf
isN   = Match 0 0 (unNs nucsN) 0xf
isGap = Match 0 0 (unNs gap)   0xf

-- | Matches anything other than gaps (but including ambiguous bases)
isAnything :: Pred Nucleotides
isAnything = notP isGap

-- | Matches any proper base (A,C,G or T)
isBase :: Pred Nucleotides
isBase = isA .||. isC .||. isG .||. isT

-- | Applies the argument to the original base (the reference)
wasP :: Pred Nucleotides -> Pred NPair
wasP = mapMatch (,,,)

-- | Applies the argument to the actual base (the query)
isP :: Pred Nucleotides -> Pred NPair
isP = mapMatch (\a b x m -> (a, b, shiftL x 4, shiftL m 4))

-- | True if applied at least @n@ bases from the left edge.
fromLeft  :: Int -> Pred a
fromLeft n = NotP $ Match (fromIntegral n) 0 (unNs gap) 0xf

-- | True if applied at least @n@ bases from the right edge.
fromRight :: Int -> Pred a
fromRight n = NotP $ Match 0 (fromIntegral n) (unNs gap) 0xf

-- | Applies the argument to the next position.
before :: Pred a -> Pred a
before = mapMatch (\a b x m -> (a, b+1, x, m))

-- | Applies the argument to the previous position.
after  :: Pred a -> Pred a
after = mapMatch (\a b x m -> (a+1, b, x, m))


data Ratio = Ratio !Int !Int deriving Show

instance Storable Ratio where
    sizeOf    ~(Ratio x y) = sizeOf x + sizeOf y
    alignment ~(Ratio x _) = alignment x
    poke p (Ratio x y) = pokeElemOff (castPtr p) 0 x >> pokeElemOff (castPtr p) 1 y
    peek p   = Ratio <$> peekElemOff (castPtr p) 0  <*> peekElemOff (castPtr p) 1

atS :: V.Vector Ratio -> Int -> Ratio
atS xs n = fromMaybe (Ratio 0 0) (xs V.!? n)


was, is :: Stylish Pred Nucleotides -> Stylish Pred NPair
was (Stylish l s c p) = Stylish ("was "++ l) s c (wasP p)
is  (Stylish l s c p) = Stylish ("is " ++ l) s c (isP  p)

no :: Stylish Pred a -> Stylish Pred a
no  (Stylish l s c p) = Stylish ("no " ++ l) s c (notP p)


(^&&), (^||) :: Stylish Pred a -> Stylish Pred a -> Stylish Pred a
(^&&) (Stylish l s c p) (Stylish m t d q) = Stylish (l++" "++m) (mplus s t) (mplus c d) $ p .&&. q
(^||) (Stylish l s c p) (Stylish m t d q) = Stylish (l++","++m) (mplus s t) (mplus c d) $ p .||. q

nA, nC, nG, nT, nN, ngap, whatever, anything :: Stylish Pred Nucleotides
nA       = Stylish "A" (Just Solid) (Just green) isA
nC       = Stylish "C" (Just Solid) (Just blue)  isC
nG       = Stylish "G" (Just Solid) (Just black) isG
nT       = Stylish "T" (Just Solid) (Just red)   isT
nN       = Stylish "N" Nothing      Nothing      isN
ngap     = Stylish "-" Nothing      Nothing      isGap
anything = Stylish "?" Nothing      Nothing      isAnything
whatever = Stylish "X" Nothing      Nothing      isBase

trimmed :: Stylish Pred a
trimmed = Stylish "trim" Nothing Nothing TrimmedP


type Curve = Stylish Curve_ Ratio
data Curve_ v = Curve { c_off       :: Int
                      , c_values    :: V.Vector v
                      } deriving Show


-- | Vector function that results in type @b@.  This is the result of
-- accumulationg a 'Pred', and it will always take vectors of 'NPair' as
-- input.  To make this an 'Applicative', it needs to store 'Pred's along
-- with Haskell functions to be applied to it.  We keep it in
-- applicative normal form.
data VFun b = VFun ([Stylish V.Vector Int] -> b) [Stylish Pred NPair]

instance Functor VFun where
    fmap f (VFun g es) = VFun (f . g) es

instance Applicative VFun where
    pure a = VFun (const a) []
    VFun f as <*> VFun g bs =
        let l = length as
        in VFun (\xs -> f (take l xs) $ g (drop l xs)) (as++bs)

-- | Accumulate a single predicate into a 'Int'.
accum :: Stylish Pred NPair -> VFun (Stylish V.Vector Int)
accum = VFun head . pure


-- | Lifts a binary function to a binary function of styled vector
-- functions.  The shorter input vector is zero padded.
liftVF2 :: (Storable a, Num a, Storable b, Num b, Storable c)
        => String -> (a -> b -> c)
        -> VFun (Stylish V.Vector a) -> VFun (Stylish V.Vector b) -> VFun (Stylish V.Vector c)
liftVF2 ops op = liftA2 $ \f g -> Stylish
    { s_label     = s_label f ++ ops ++ s_label g
    , s_linestyle = s_linestyle f `mplus` s_linestyle g
    , s_linecolor = s_linecolor f `mplus` s_linecolor g
    , s_payload   = do_zip (s_payload f) (s_payload g) }
  where
    do_zip v w = V.zipWith op (v V.++ V.replicate (max 0 (V.length w - V.length v)) 0)
                              (w V.++ V.replicate (max 0 (V.length v - V.length w)) 0)

(^+), (^-), (^*) :: (Num a, Storable a)
                 => VFun (Stylish V.Vector a) -> VFun (Stylish V.Vector a) -> VFun (Stylish V.Vector a)
(^+) = liftVF2 "+" (+)
(^-) = liftVF2 "-" (-)
(^*) = liftVF2 "*" (*)

(^/) :: (Integral a, Storable a, Integral b, Storable b)
     => VFun (Stylish V.Vector a) -> VFun (Stylish V.Vector b) -> VFun (Stylish V.Vector Ratio)
(^/) = liftVF2 "/" (\x y -> Ratio (fromIntegral x) (fromIntegral y))


substitutions :: VFun [Stylish V.Vector Ratio]
substitutions = sequenceA
    [ numer ^/ denom
    | (x,ys,ss) <- zip3
            [ nA, nC, nG, nT ]
            [[ nC, nG, nT ], [ nA, nG, nT ], [ nA, nC, nT ], [ nA, nC, nG ]]
            [[ (Dashed dash, (int,int,1)),  (Dashed dash, (int,int,int)),     (Dashed dash, (int,1,1))        ]
            ,[ (Solid, (1,int,1)),          (Solid, (int,int,int)),           (Solid, (1,0,0))                ]
            ,[ (Solid, (0,1,0)),            (Dashed dotted,  (1,  0.7, int)), (Dashed dotted, (int,1,1))      ]
            ,[ (Dashed dashdot, (1,int,1)), (Dashed dashdot, (1,0.7, int)),   (Dashed dashdot, (int,int,int)) ]]

    , let denom = accum $ was x ^&& Stylish "" Nothing Nothing (fromRight 25)
    , (y,(s,c)) <- zip ys ss
    , let l = s_label x ++ "\8594" ++ s_label y
    , let numer = accum $ lbl l $ styled s $ colored c $ was x ^&& is y ^&& Stylish "" Nothing Nothing (fromRight 25) ]
  where
    int = 0.3 ; dash = [3,3] ; dotted = [1,2] ; dashdot = [3,2,1,2]

