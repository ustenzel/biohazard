module Bio.Adna.Query.Parser ( parseExpr ) where

import Bio.Adna                           ( NPair )
import Bio.Prelude
import Bio.Adna.Query.Language
    ( Stylish(..), LineStyle(..), mapStylish, unstyled, colored, black, red, green, blue,
    VFun, Ratio, (^+), (^-), (^*), (^/), accum,
    Pred, trimmed, whatever, anything, nA, nC, nG, nT, nN, ngap,
    is, was, fromLeft, fromRight, after, before, mapStylish, (^&&), (^||), no, lbl, styled )
import Text.LLParser

import qualified Data.Vector.Storable  as V

-- ^ Concrete syntax for the DSL; lightweight, so it can be run from the
-- command line.  The final expression computes a list of 'Curve's.

type P = Parser Bytes

data ParseError = ParseError [String] !Bytes deriving Show
instance Exception ParseError where
    displayException (ParseError ss r) =
        "Parse error at " ++ show r ++ ", expected " ++ intercalate " or " (nubHash ss) ++ "."


-- | Parses a command line argument.  Wonky unicode support:  we get a
-- 'String', but in Unix, that's really just bytes from the command
-- line.  So we coerce to 'Bytes', then call 'decodeBytes', which at
-- least works on utf8 and iso-8859-1.
parseExpr :: MonadThrow m => String -> m (VFun [Stylish V.Vector Ratio])
parseExpr = either (throwM . uncurry ParseError) (pure . fst) . pRun (pSpace *> p_functions <* pEndOfString) . fromString


-- Styling can be applied to atoms of all types.  We simply let it bind
-- more tightly than all infix ops, so it is never applied to anything
-- else.
p_styled :: P (Stylish f a -> Stylish f a)
p_styled =
    foldr (.) id <$>
    many ( asum [ colored red   <$ lit "red"
                , colored green <$ lit "green"
                , colored blue  <$ lit "blue"
                , colored black <$ lit "black"
                , colored <$> hexcolor
                , styled Solid <$ lit "solid"
                , styled (Dashed [3,3]) <$ lit "dashed"
                , styled (Dashed [1,2]) <$ lit "dotted"
                , styled (Dashed [3,2,1,2]) <$ lit "dashdot"
                , styled (Dashed [3,2,1,2,1,2]) <$ lit "dashdotdot"
                , lbl <$> pStringLit ] )

hexcolor :: P (Double,Double,Double)
hexcolor = (,,) <$ pChar8 '#' <*> primcol <*> primcol <*> primcol <* pSpace
  where
    primcol = (\x y -> fromIntegral (x * 16 + y) / 255) <$> hex <*> hex
    hex = asum [          (subtract (ord '0') . ord) <$> pTestChar8 (\c -> '0' <= c && c <= '9')
               , ((+) 10 . subtract (ord 'a') . ord) <$> pTestChar8 (\c -> 'a' <= c && c <= 'f')
               , ((+) 10 . subtract (ord 'A') . ord) <$> pTestChar8 (\c -> 'A' <= c && c <= 'F') ]

unary_ops :: P (Stylish Pred a) -> P (Stylish Pred a)
unary_ops p = asum
    [ no                   <$ lit "no"     <*> p
    , mapStylish before    <$ lit "before" <*> p
    , mapStylish after     <$ lit "after"  <*> p
    , unstyled . fromLeft  <$ lit "fromL"  <*> pInt <* pSpace
    , unstyled . fromRight <$ lit "fromR"  <*> pInt <* pSpace ]

p_atom_np :: P (Stylish Pred NPair)
p_atom_np = p_styled <*> asum
    [ was     <$ lit "was" <*> p_atom_ns
    , is      <$ lit "is"  <*> p_atom_ns
    , trimmed <$ lit "trimmed"
    , unary_ops p_atom_np ]
  <?> "predicate on nucleotide pairs"

p_bool_expr :: P (Stylish Pred a) -> P (Stylish Pred a)
p_bool_expr p_prim = p_disj
  where
    p_disj = pChainl ((^||) <$ pLiteral "|") p_conj
    p_conj = pChainl ((^&&) <$ pLiteral "&") (p_atom <|> p_prim)
    p_atom = lit "(" *> p_bool_expr p_prim <* lit ")"

p_atom_ns :: P (Stylish Pred Nucleotides)
p_atom_ns = p_styled <*> asum
    [ nA       <$ lit "A"
    , nC       <$ lit "C"
    , nG       <$ lit "G"
    , nT       <$ lit "T"
    , nN       <$ lit "N"
    , ngap     <$ lit "gap"
    , anything <$ lit "any"
    , whatever <$ lit "X"
    , trimmed  <$ lit "trimmed"
    , unary_ops p_atom_ns ]
  <?> "predicate on nucleotide pairs"

lit :: Bytes -> P ()
lit s = () <$ pLiteral s *> pSpace <?> unpack s

p_functions :: P (VFun [Stylish V.Vector Ratio])
p_functions = sequenceA <$> pChainr ((++) <$ pLiteral ",") (pure <$> p_function)

p_function :: P (VFun (Stylish V.Vector Ratio))
p_function = fmap <$> p_styled <*>
                ( lit "(" *> p_quot <* lit ")" <|> p_quot )
  where
    p_quot :: P (VFun (Stylish V.Vector Ratio))
    p_quot = (^/) <$> p_prod <* lit "/" <*> p_prod

    p_prod, p_sum, p_fatom :: P (VFun (Stylish V.Vector Int))
    p_prod  = pChainl ((^*) <$ pLiteral "*") p_sum
    p_sum   = flip ($) <$> p_fatom <*> asum
                [ flip (^+) <$ lit "+" <*> p_sum
                , flip (^-) <$ lit "-" <*> p_sum
                , pure id ]
    p_fatom = fmap <$> p_styled <*>
                ( lit "(" *> p_prod <* lit ")" <|>
                  pure accum <*> p_bool_expr p_atom_np )

