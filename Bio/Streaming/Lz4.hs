module Bio.Streaming.Lz4 ( lz4, unLz4, builderToLz4ByteStream, chunkSize ) where

import Bio.Prelude
import Foreign.C.Types                  ( CChar(..), CInt(..) )
import qualified Bio.Streaming.Bytes              as S
import qualified Data.ByteString                  as B
import qualified Data.ByteString.Builder          as B
import qualified Data.ByteString.Builder.Internal as B ( untrimmedStrategy )
import qualified Data.ByteString.Internal         as B ( createAndTrim )
import qualified Data.ByteString.Unsafe           as B ( unsafeUseAsCStringLen )


-- | The maximum chunk size we use in compression.  It works best if the
-- incoming stream has chunks near this size, but no bigger.  Must be
-- less than 2^16 for our frame format to work.
chunkSize :: Int
chunkSize = 0xfeff00

-- | The minimum chunk size for compression.  If a chunk is smaller than
-- this and concatening it with the next chunk does not exceed
-- chunkSize, they are coalesced.
smallSize :: Int
smallSize = 0xfef00

-- | The buffer size when compressing.  It's sufficiently bigger than
-- the chunkSize to fit even incompressible chunks.
bufSize :: Int
bufSize = 0xffff80

-- | Compresses a 'S.ByteStream'.  We normally compress and frame each
-- 'Chunk' individually, which is good if they come in reasonable sizes.
-- However, we enforce a maximum size to make decompression easier, and
-- we concatenate small chunks to keep overhead low.
lz4 :: MonadIO m => S.ByteStream m r -> S.ByteStream m r
lz4 = lift . S.unconsChunk >=> either pure (uncurry go)
  where
    go s ss
        | B.null s               = lz4 ss
        | B.length s < smallSize = lift (S.unconsChunk ss) >>= go_s s
        | B.length s > chunkSize = do s' <- liftIO $ comp (B.take chunkSize s)
                                      S.consChunk s' $ go (B.drop chunkSize s) ss
        | otherwise              = do s' <- liftIO $ comp s
                                      S.consChunk s' $ lz4 ss

    go_s s1 (Left r)                            = do s1' <- liftIO $ comp s1
                                                     S.consChunk s1' (pure r)
    go_s s1 (Right (s2,ss))
        | B.length s1 + B.length s2 < smallSize =    lift (S.unconsChunk ss) >>= go_s (s1 <> s2)
        | B.length s1 + B.length s2 > chunkSize = do s1' <- liftIO $ comp s1
                                                     S.consChunk s1' $ go s2 ss
        | otherwise                             = do s' <- liftIO $ comp (s1 <> s2)
                                                     S.consChunk s' $ lz4 ss

    comp s = B.createAndTrim bufSize $ \pdest ->
             B.unsafeUseAsCStringLen s $ \(psrc,srcLen) -> do
                dLen <- c_lz4 psrc (plusPtr pdest 3) (fromIntegral srcLen) (fromIntegral bufSize)
                -- place compressed size at beginning
                pokeByteOff pdest 0 (fromIntegral (dLen `shiftR`  0) :: Word8)
                pokeByteOff pdest 1 (fromIntegral (dLen `shiftR`  8) :: Word8)
                pokeByteOff pdest 2 (fromIntegral (dLen `shiftR` 16) :: Word8)
                return $ fromIntegral dLen + 3


-- | Decompresses a 'S.ByteStream' that was compressed by 'lz4'.
unLz4 :: MonadIO m => S.ByteStream m r -> S.ByteStream m r
unLz4 s0 = one_byte s0 $ \(x0,_,s1) ->
           one_byte s1 $ \(x1,_,s2) ->
           one_byte s2 $ \(x2,o,s3) ->
           go (fromIntegral x0 `shiftL` 0 .|. fromIntegral x1 `shiftL` 8 .|. fromIntegral x2 `shiftL` 16) mempty o s3
  where
    one_byte s k = lift (S.unconsOff s) >>= either pure k

    go l c o s
        | l > 0     = lift (S.unconsChunk s) >>= either pure (\(c',s') ->
                      go (l - B.length c') (c <> B.take l c') o (S.consChunkOff (B.drop l c) (o + fromIntegral (B.length c + l)) s'))

        | otherwise = do ck' <- liftIO $ B.createAndTrim chunkSize         $ \pdst ->
                                         B.unsafeUseAsCStringLen c         $ \(psrc,srcLen) ->
                                         fromIntegral <$> c_unlz4 psrc pdst (fromIntegral srcLen) (fromIntegral chunkSize)
                         S.consChunkOff ck' (shiftL o 24) (unLz4 s)


-- | Converts a 'B.Builder' to a 'S.ByteStream' using a reasonable chunk
-- size, then compresses it using LZ4.
builderToLz4ByteStream :: MonadIO m => B.Builder -> S.ByteStream m ()
builderToLz4ByteStream = lz4 . S.toByteStreamWith (B.untrimmedStrategy chunkSize chunkSize)


foreign import ccall unsafe "lz4.h LZ4_compress_default"
    c_lz4 :: Ptr CChar  -- source
          -> Ptr CChar  -- dest
          -> CInt       -- sourceSize
          -> CInt       -- maxDestSize
          -> IO CInt    -- number of bytes written

foreign import ccall unsafe "lz4.h LZ4_decompress_safe"
    c_unlz4 :: Ptr CChar  -- source
            -> Ptr Word8  -- dest
            -> CInt       -- sourceSize
            -> CInt       -- maxDestSize
            -> IO CInt    -- number of bytes written
