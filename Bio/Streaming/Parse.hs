{-# LANGUAGE Rank2Types #-}
-- | Parsers for use with 'S.ByteStream's.
module Bio.Streaming.Parse
    ( Parser
    , EofException(..)
    , parse
    , parseIO
    , parseLog
    , parseM
    , abortParse
    , isFinished
    , dropBytes
    , dropLine
    , getByte
    , getString
    , getWord32
    , getWord64
    , liftFun
    ) where

import Bio.Prelude
import Streaming                                ( lazily )

import qualified Bio.Streaming.Bytes            as S
import qualified Data.ByteString                as B

newtype Parser r m a = P {
    runP :: forall x .
            (a -> S.ByteStream m r -> m x)              -- successful parse
         -> (r -> m x)                                  -- end of input stream
         -> (SomeException -> S.ByteStream m r -> m x)  -- exception and remaining input
         -> S.ByteStream m r -> m x }                   -- input, result

instance Functor (Parser r m) where
    fmap f p = P $ \sk -> runP p (sk . f)

instance Applicative (Parser r m) where
    pure a = P $ \sk _rk _ek -> sk a
    a <*> b = P $ \sk rk ek -> runP a (\f -> runP b (\x -> sk (f x)) rk ek) rk ek

instance Monad (Parser r m) where
    return = pure
    m >>= k = P $ \sk rk ek -> runP m (\a -> runP (k a) sk rk ek) rk ek

instance MonadIO m => MonadIO (Parser r m) where
    liftIO m = P $ \sk _rk _ek s -> liftIO m >>= \a -> sk a s

instance MonadTrans (Parser r) where
    lift m = P $ \sk _rk _ek s -> m >>= \a -> sk a s

instance MonadThrow (Parser r m) where
    throwM e = P $ \_sk _rk ek -> ek (toException e)

instance PrimMonad m => PrimMonad (Parser r m) where
    type PrimState (Parser r m) = PrimState m
    primitive = lift . primitive


modify :: (S.ByteStream m r -> S.ByteStream m r) -> Parser r m ()
modify f = P $ \sk _rk _ek -> sk () . f

parse :: Monad m => (Int64 -> Parser r m a) -> S.ByteStream m r
      -> m (Either (SomeException, S.ByteStream m r) (Either r (a, S.ByteStream m r)))
parse p = go
  where
    go    (S.Empty     r)             = return $ Right $ Left r
    go    (S.Go        k)             = k >>= go
    go ck@(S.Chunk c o s) | B.null  c = go s
                          | otherwise = runP (p o) (\a t -> return . Right $ Right (a,t))
                                                   (return . Right . Left)
                                                   (curry $ return . Left)
                                                   ck

parseIO :: MonadIO m => (Int64 -> Parser r m a) -> S.ByteStream m r -> m (Either r (a, S.ByteStream m r))
parseIO p = parse p >=> either (liftIO . throwM . fst) return

parseLog :: MonadLog m => Level -> (Int64 -> Parser r m a) -> S.ByteStream m r -> m (Either r (a, S.ByteStream m r))
parseLog lv p = parse p >=> either throw_it pure
  where throw_it (ex,rest) = logMsg lv ex >> Left <$> S.effects rest

parseM :: MonadThrow m => (Int64 -> Parser r m a) -> S.ByteStream m r -> m (Either r (a, S.ByteStream m r))
parseM p = parse p >=> either (throwM . fst) return

abortParse :: Monad m => Parser r m a
abortParse = P $ \_sk rk _ek -> S.effects >=> rk

liftFun :: Monad m => (S.ByteStream m r -> m (Of a (S.ByteStream m r))) -> Parser r m a
liftFun f = P $ \sk _rk _ek -> f >=> uncurry sk . lazily

isFinished :: Monad m => Parser r m Bool
isFinished = liftFun go
  where
    go    (S.Empty     r)             = return (True :> S.Empty r)
    go    (S.Go        k)             = k >>= go
    go ck@(S.Chunk c _ s) | B.null  c = go s
                          | otherwise = return (False :> ck)

dropBytes :: Monad m => Int -> Parser r m ()
dropBytes l = modify $ S.drop (fromIntegral l)

dropLine :: Monad m => Parser r m ()
dropLine = modify $ S.drop 1 . S.dropWhile (/= 10)

getByte :: Monad m => Parser r m Word8
getByte = P $ \sk _rk ek -> S.uncons >=> either (ek (toException EofException) . pure) (uncurry sk)

getString :: Monad m => Int -> Parser r m B.ByteString
getString = liftFun . S.splitAt'

getWord32 :: Monad m => Parser r m Word32
getWord32 = liftM (fst . B.foldl (\(a,i) w -> (a + shiftL (fromIntegral w) i, i + 8)) (0,0)) (getString 4)

getWord64 :: Monad m => Parser r m Word64
getWord64 = liftM (fst . B.foldl (\(a,i) w -> (a + shiftL (fromIntegral w) i, i + 8)) (0,0)) (getString 8)


data EofException = EofException deriving (Show, Typeable)
instance Exception EofException where displayException _ = "end-of-file"

