module Bio.Streaming.Vector ( concatVectors, stream2vector, stream2vectorN ) where

import Bio.Prelude
import Streaming

import qualified Data.Vector.Generic            as VG
import qualified Data.Vector.Generic.Mutable    as VM

-- | Equivalent to @stream2vector . Streaming.Prelude.take n@, but
-- allocates memory correctly and is thereby more efficient.
stream2vectorN :: (PrimMonad m, VG.Vector v a) => Int -> Stream (Of a) m b -> m (v a)
stream2vectorN n s0 = do
    mv <- VM.new n
    go mv 0 s0
  where
    go !mv !i s
        | i == n    = VG.unsafeFreeze mv
        | otherwise =
            inspect s >>= \case
                Left      _     -> VG.unsafeFreeze $ VM.take i mv
                Right (a :> s') -> VM.write mv i a >> go mv (i+1) s'

-- | Reads the whole stream into a 'VG.Vector'.
stream2vector :: (PrimMonad m, VG.Vector v a) => Stream (Of a) m r -> m (Of (v a) r)
stream2vector s0 = do
    mv <- VM.new 1024
    go mv 0 s0
  where
    go !mv !i =
        inspect >=> \case
            Left        r  -> liftM (:> r) $ VG.unsafeFreeze $ VM.take i mv
            Right (a :> s) -> do mv' <- if VM.length mv == i then VM.grow mv (VM.length mv) else return mv
                                 VM.write mv' i a
                                 go mv' (i+1) s

concatVectors :: (VG.Vector v a, PrimMonad m) => Stream (Of (v a)) m r -> m (Of (VG.Mutable v (PrimState m) a) r)
concatVectors s = do v <- VM.unsafeNew 1024 ; go v 0 s
  where
    go !v !i = inspect >=> \case
        Left    r       -> return (VM.take i v :> r)
        Right (x :> xs) -> do
            let lx = VG.length x
            v' <- if i + lx <= VM.length v
                    then return v
                    else VM.grow v (VM.length v `max` lx)
            VM.move (VM.slice i lx v') =<< VG.unsafeThaw x
            go v' (i + lx) xs
{-# INLINABLE concatVectors #-}
