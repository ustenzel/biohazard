module Data.PriorityQueue.External (
        Extern(..),
        get_byte_string,
        put_byte_string,
        fdPut,

        PQ_Conf(..),
        PQ,
        makePQ,
        closePQ,
        enqueuePQ,
        viewMinPQ,
        sizePQ,
        listToPQ,
        pqToStream
) where

import Bio.Prelude
import Bio.Streaming
import Bio.Streaming.Lz4
import Foreign.C.Error                  ( throwErrnoIfMinus1, throwErrnoIf_ )
import Foreign.C.String                 ( withCAString )
import Foreign.C.Types                  ( CChar(..), CInt(..), CSize(..) )
import System.Posix.IO
import System.Posix.Types

import qualified Bio.Streaming.Bytes              as S
import qualified Bio.Streaming.Parse              as P
import qualified Bio.Streaming.Prelude            as Q
import qualified Data.ByteString                  as B
import qualified Data.ByteString.Builder          as B
import qualified Data.ByteString.Internal         as B ( createAndTrim )
import qualified Data.ByteString.Unsafe           as B ( unsafeUseAsCStringLen )


-- | A Priority Queue that can fall back to external storage.
--
-- Note that such a Priority Queue automatically gives rise to an
-- external sorting algorithm:  enqueue everything, dequeue until empty.
--
-- Whatever is to be stored in this queue needs to be an instance of
-- 'Extern', because it may need to be moved to external storage on
-- demand.  We also need a way to estimate the memory consumption of an
-- enqueued object.  When constructing the queue, the maximum amount of
-- RAM to consume is set.  Note that open input streams use memory for
-- buffering, too, this should be taken into account, at least
-- approximately.
--
-- Enqueued objects are kept in an in-memory heap until the memory
-- consumption becomes too high.  At that point, the whole heap is
-- sorted and dumped to external storage.  If necessary, the file to do
-- so is created and kept open.  The newly created stream is added to
-- another heap, so that dequeueing objects amounts to performing a
-- merge sort on multiple, possibly external, streams.  If too many
-- streams are open, we do exactly that:  merge-sort all streams in one
-- generation into a single stream in the next generation.  The discrete
-- generations ensure that merging handles streams of roughly equal
-- length.  To conserve on file descriptors, we concatenate multiple
-- streams into a single file, then use pread(2) on that as appropriate.
-- This way, we need only one file descriptor per generation.
--
-- A generic way to decide when too many streams are open or how much
-- memory we can afford to use would be nice.  That first condition is
-- reached when seeking takes about as much time as reading (which
-- depends on buffer size and system characteristics), so that an
-- additional merge pass becomes economical.  The second condition not
-- only depends on how much memory we have, but also on how much we will
-- use for the merging logic.  Right now, both are just a parameter.

data PQ_Conf m = PQ_Conf {
        max_mb :: Int,          -- ^ memory limit
        max_merge :: Int,       -- ^ fan-in limit
        temp_path :: FilePath,  -- ^ path to temporary files
        croak :: String -> m () }

data PQ a = PQ { heap       :: !(SkewHeap a)        -- generation 0 (in RAM)
               , heap_size  :: !Int                 -- approx. size of gen 0
               , sizePQ     :: !Int                 -- number of items in total
               , spills     :: [Spill a] }
  deriving Show

data Spill a = Spill Fd (SkewHeap (Stream1 a)) deriving Show

-- | Things that can be externalized.
class Extern a where
    -- | A (conservative) size estimate.  Doesn't need to be accurate,
    -- it's used to decide when to spill to external memory.
    usedBytes :: a -> Int

    -- | A way to serialize.
    extern :: a -> B.Builder

    -- | A way to deserialize.
    intern :: Monad m => P.Parser r m a


-- | Creates a priority queue.  Note that the priority queue creates
-- files, which will only be cleaned up if deletePQ is called.
makePQ :: PQ a
makePQ = PQ Empty 0 0 []

-- | Closes open file descriptors.  There is only one way file
-- descriptors become detectably unused, which is when we migrate one
-- generation of temporary streams to the next.  In this case, the 'Fd'
-- is closed.  Therefore, when we are done with a PQ, we have to close
-- these file handles manually.  (Naturally, the 'PQ' can no longer be
-- used after calling this.)
closePQ :: MonadIO m => PQ a -> m ()
closePQ = liftIO . mapM_ (\(Spill fd _) -> closeFd fd) . spills

-- | Enqueues an element.
-- This operation may result in the creation of a file or in an enormous
-- merge of already created files.
enqueuePQ :: (Ord a, Extern a, MonadIO m) => PQ_Conf m -> a -> PQ a -> m (PQ a)
enqueuePQ cfg a (PQ p s c ss) = do
    let !msz = 1000000 * max_mb cfg
        !s' = s + usedBytes a
        !pq' = PQ (insertH a p) s' (succ c) ss
    if s' >= msz
      then flushPQ cfg pq'
      else return pq'

listToPQ :: (Ord a, Extern a, MonadIO m) => PQ_Conf m -> [a] -> m (PQ a)
listToPQ cfg = foldM (flip (enqueuePQ cfg)) makePQ


-- | Takes 99% of the in-memory portion of the PQ and flushes it to
-- generation one in external storage.

flushPQ :: (Ord a, Extern a, MonadIO m) => PQ_Conf m -> PQ a -> m (PQ a)
flushPQ cfg PQ{..} = do
    croak cfg "Spilling memory to gen 0."
    let msz = 10000 * max_mb cfg
        (sheap, ssize, lheap) = splitH msz Empty 0 heap
    spills' <- spillTo cfg 0 (streamHeap lheap) spills
    return $! PQ sheap ssize sizePQ spills'
  where
    -- We take up to 1% of the maximum size off generation 0 and make that the
    -- new generation 0.  The remainder is flushed.  The idea is that the next
    -- items may be extracted soon, so it wouldn't pay to flush them.
    splitH msz sh ssz h = case viewMin h of
        Just (b,h') | usedBytes b + ssz < msz ->
            splitH msz (insertH b sh) (usedBytes b + ssz) h'
        _ -> (sh, ssz, h)

-- Spill a list of thingies to external storage.  A list of generations
-- of external spills is supplied.  We always spill to the first
-- generation by appending to the 'Fd' and creating a new stream that is
-- added to the 'SkewHeap'.  If the list is empty, we have to create the
-- first generation spill.
--
-- After spilling, we check if the first generation overflowed.  If
-- so, we merge by spilling it to the tail of the list and recreating a
-- new, empty first generation.

spillTo :: (Ord a, Extern a, MonadIO m) => PQ_Conf m -> Int -> Stream (Of a) IO () -> [Spill a] -> m [Spill a]
spillTo cfg g as [                         ] = mkEmptySpill cfg >>= spillTo cfg g as . (:[])
spillTo cfg g as (Spill fd streams : spills) = do
    str <- liftIO . externalize fd . S.concatBuilders . Q.map extern $ as
    streams' <- insertHS (Q.unfoldr (P.parseIO (const intern)) str) streams
    pos <- liftIO $ fdSeek fd RelativeSeek 0
    croak cfg $ "Gen " ++ shows g " has " ++ shows (sizeH streams') " streams (fd "
             ++ shows fd ", " ++ shows (pos `shiftR` 20) "MB)."
    if sizeH streams' == max_merge cfg
      then do croak cfg $ "Spilling gen " ++ shows g " to gen " ++ shows (succ g) "."
              (:) <$> mkEmptySpill cfg
                  <*> spillTo cfg (succ g) (Q.unfoldr viewMinS streams') spills
                  <*  liftIO (closeFd fd)
      else return $ Spill fd streams' : spills

mkEmptySpill :: MonadIO m => PQ_Conf m -> m (Spill a)
mkEmptySpill cfg = liftIO $ do pn <- getProgName
                               fd <- withCAString (temp_path cfg ++ "/" ++ pn ++ "-XXXXXX") $ \p ->
                                     throwErrnoIfMinus1 "mkstemp" (mkstemp p) <* unlink p
                               return $ Spill fd Empty


-- | Creates a disk backed stream from a heap.  The heap is unravelled
-- in ascending order and written to a new segment in a temporary file.
-- That segment is then converted back to a 'ByteStream', which is
-- returned.
externalize :: MonadIO m => Fd -> B.Builder -> m (ByteStream m ())
externalize fd s = do
        pos0 <- liftIO $ fdSeek fd RelativeSeek 0
        liftIO . S.mapChunksM_ (fdPut fd) $ builderToLz4ByteStream s
        pos1 <- liftIO $ fdSeek fd RelativeSeek 0
        return $ unLz4 $ fdGetStream fd pos0 pos1

fdGetStream :: MonadIO m => Fd -> FileOffset -> FileOffset -> ByteStream m ()
fdGetStream fd p0 p1
    | p0 == p1  = pure ()
    | otherwise = do
        let len = (p1-p0) `min` (1024*1024)
        str <- liftIO $ B.createAndTrim (fromIntegral len) $ \p ->
                        fmap fromIntegral $
                            throwErrnoIfMinus1 "pread" $
                                pread fd p (fromIntegral len) p0

        S.consChunkOff str (fromIntegral p0) $
            fdGetStream fd (p0 + fromIntegral (B.length str)) p1

foreign import ccall unsafe "stdlib.h mkstemp" mkstemp :: Ptr CChar -> IO Fd
foreign import ccall unsafe "unistd.h unlink"  unlink  :: Ptr CChar -> IO CInt
foreign import ccall unsafe "unistd.h pread"   pread   :: Fd -> Ptr a -> CSize -> FileOffset -> IO CSsize


testStream :: (Monad m, Ord a) => Stream (Of a) m r -> Stream (Of a) m r
testStream = lift . inspect >=> \case
    Left r -> pure r
    Right (a :> s) -> go a s
  where
    go a = lift . inspect >=> \case
        Left r -> wrap (a :> pure r)
        Right (b :> s)
            | a <= b    -> wrap (a :> go b s)
            | otherwise -> error "internal error:  sorting violated?!"

viewMinPQ :: (Ord a, Extern a) => PQ a -> Maybe (a, IO (PQ a))
viewMinPQ PQ{..} =
    case viewMinL spills of

        Just (a, ss') -> case viewMin heap of

            Just (b, h') | b <= a ->
                Just (b, return $ PQ h' (heap_size - usedBytes b) (pred sizePQ) spills)

            _ -> Just (a, PQ heap heap_size (pred sizePQ) <$> ss')

        Nothing -> case viewMin heap of

            Just (b ,h') ->
                Just (b, return $ PQ h' (heap_size - usedBytes b) (pred sizePQ) spills)

            Nothing -> Nothing

-- | Minimum view of a list of spills.  Looking at the minimum is
-- possible without side effects, but dropping it is not.  Hence, we
-- return a pair of the minimum element and an action that will drop it.
viewMinL :: Ord a => [Spill a] -> Maybe (a, IO [Spill a])
viewMinL = mmin . go []
  where
    mmin [] = Nothing
    mmin xs = Just $ minimumBy (comparing fst) xs

    go _acc [    ] = []
    go  acc (s:ss) = case s of
        Spill _fd  Empty               -> go (s:acc) ss
        Spill  fd (Node (S1 a as) l r) -> (a, drop_a) : go (s:acc) ss
          where
            drop_a = insertHS as (unionH l r) >>= \case
                        Empty -> closeFd fd >> return (reverse acc ++ ss)
                        h     -> return (reverse acc ++ Spill fd h : ss)

pqToStream :: (MonadIO m, Ord a, Extern a) => PQ a -> Stream (Of a) m ()
pqToStream = testStream . go
  where
    go pq = case viewMinPQ pq of
        Nothing      -> pure ()
        Just (a,pq') -> wrap (a :> (liftIO pq' >>= go))

-- We need an in-memory priority queue.  Here's a skew heap.
data SkewHeap a = Empty | Node a (SkewHeap a) (SkewHeap a) deriving Show

singleton :: a -> SkewHeap a
singleton x = Node x Empty Empty

unionH :: Ord a => SkewHeap a -> SkewHeap a -> SkewHeap a
Empty              `unionH` t2                 = t2
t1                 `unionH` Empty              = t1
t1@(Node x1 l1 r1) `unionH` t2@(Node x2 l2 r2)
   | x1 <= x2                                 = Node x1 (t2 `unionH` r1) l1
   | otherwise                                = Node x2 (t1 `unionH` r2) l2

insertH :: Ord a => a -> SkewHeap a -> SkewHeap a
insertH x heap = singleton x `unionH` heap

insertHS :: (Ord a, MonadIO m) => Stream (Of a) IO () -> SkewHeap (Stream1 a) -> m (SkewHeap (Stream1 a))
insertHS a h = liftIO (inspect a) >>= \case
    Left     ()     -> return h
    Right (b :> s') -> return $ insertH (S1 b s') h


viewMin :: Ord a => SkewHeap a -> Maybe (a, SkewHeap a)
viewMin Empty        = Nothing
viewMin (Node x l r) = Just (x, unionH l r)

viewMinS :: (Ord a, MonadIO m) => SkewHeap (Stream1 a) -> m (Either () (a, SkewHeap (Stream1 a)))
viewMinS h =
    case viewMin h of
        Nothing           -> return $ Left ()
        Just (S1 a s, h') -> Right . (,) a <$> insertHS s h'

sizeH :: SkewHeap a -> Int
sizeH  Empty       = 0
sizeH (Node _ l r) = sizeH l + sizeH r + 1

streamHeap :: (Extern a, Ord a, Monad m) => SkewHeap a -> Stream (Of a) m ()
streamHeap h = case viewMin h of
    Nothing     -> pure ()
    Just (a,h') -> wrap (a :> streamHeap h')


data Stream1 a = S1 a (Stream (Of a) IO ())

instance Show a => Show (Stream1 a) where
    show (S1 a _) = "S1 (" ++ shows a ")"

-- Streams are ordered by looking at just the first item.
instance Eq a => Eq (Stream1 a) where
    S1 a _ == S1 b _  =  a == b

instance Ord a => Ord (Stream1 a) where
    S1 a _ `compare` S1 b _  =  compare a b


put_byte_string :: Bytes -> B.Builder
put_byte_string s = B.word32LE (fromIntegral (B.length s)) <> B.byteString s

get_byte_string :: Monad m => P.Parser r m Bytes
get_byte_string = P.getWord32 >>= P.getString . fromIntegral

fdPut :: Fd -> Bytes -> IO ()
fdPut fd s = B.unsafeUseAsCStringLen s $ \(p,l) ->
             throwErrnoIf_ (/= fromIntegral l) "fdPut" $
             fdWriteBuf fd (castPtr p) (fromIntegral l)

