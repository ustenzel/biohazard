module Data.IntervalMap (
        IntervalMap(..), empty, insert, deleteBy, size, fromList, toList,
        lookup, lookupWith, lookupWithKeys,
        lookupOverlap, lookupOverlapWith, lookupOverlapWithKeys,
        lookupContained, lookupContainedWith, lookupContainedWithKeys,
        lookupContaining, lookupContainingWith, lookupContainingWithKeys,
        lookupPartial, lookupPartialWith, lookupPartialWithKeys,
        lookupLeft, lookupRight,
        leftmost, rightmost, i2w, w2i
    ) where

-- ^ Interval map, implemented as a big endian PATRICIA trie.
--
-- This implements a multimap from half-open, non-empty intervals on
-- 'Word' to arbitrary values.  We first trie on the left key, where
-- every node stores a common prefix, which is also a split key for the
-- left edges, and a maximum for the right edges.  This forms a priority
-- key.  Below that, we trie on the right key, and get a search tree for
-- intervals starting at the same position.
--
-- XXX  The m constructor doesn't need h anymore, does it?
--
-- XXX  Some opportunities for pruning the searches may have been
--      missed.
--
-- Woo-hoo!  The speed is finally competitive with Judy, but needs a bit
-- more memory (35% more).  However, it's *much* cleaner, which makes
-- using it worthwhile.  This is very useful to represent BED files and
-- the like.
--
-- Notes on possible performance improvements:
--
-- - passing the redundantly stored 'min' down the search functions
--   provides negligible benefit
-- - passing both 'min' and 'max' causes extreme slowdown

import Bio.Prelude hiding ( deleteBy, empty, fromList, insert, lookup, toList )

type Prefix = Word
type Mask = Word
type Key = Word

-- | A map from integer intervals to values.  Invariants:
-- * 'E' does not occur below any other constructor.
-- * 'N' does not occur below 'M'.
-- * If @L a b _ _@ is stored in @t1@ of @N k h _ t1 t2@, then @a < k@;
--   if it is stored in @t2@, then @a >= k@.  Either way, @b <= h@.
-- * If @L a b _ _@ is stored in @t1@ of @M k h _ t1 t2@, then @b < k@;
--   if it is stored in @t2@, then @b >= k@.  Either way, @b <= h@.

data IntervalMap a = E
                   -- | A leaf.  Leaves cannot be empty, but they can
                   -- contain more than one value
                   | L {-# UNPACK #-} !Key {-# UNPACK #-} !Key a [a]        -- ^ an interval with payload
                   -- | A split node.  All left keys match the common
                   -- prefix up to, but not including the mask bit; the
                   -- mask bit is cleared in all left keys of the let
                   -- subtree and set in all left keys of the right
                   -- subtree.
                   | N {-# UNPACK #-} !Prefix                               -- ^ the common prefix
                       {-# UNPACK #-} !Key                                  -- ^ the maximum right key
                       {-# UNPACK #-} !Mask                                 -- ^ the mask; exactly one bit is set
                       !(IntervalMap a)                                     -- ^ left subtree
                       !(IntervalMap a)                                     -- ^ right subtree
                   -- | A split node on the right key.  All left keys
                   -- are identical, and all right keys match the common
                   -- prefix up to, but not including the mask bit; the
                   -- mask bit is cleared in all right keys of the left
                   -- subtree and set in all right keys of the right
                   -- subtree.
                   | M {-# UNPACK #-} !Prefix                               -- ^ the common prefix
                       {-# UNPACK #-} !Key                                  -- ^ the minimum left key
                       {-# UNPACK #-} !Key                                  -- ^ the maximum right key
                       {-# UNPACK #-} !Mask                                 -- ^ the mask; exactly one bit is set
                       !(IntervalMap a)                                     -- ^ left subtree
                       !(IntervalMap a)                                     -- ^ right subtree
  deriving (Show, Read)

i2w :: Int -> Word
i2w = xor highest_bit . fromIntegral

w2i :: Word -> Int
w2i = fromIntegral . xor highest_bit

highest_bit :: Word
highest_bit = shiftR maxBound 1 + 1

-- | Creates a new, empty 'IntervalMap'.
empty :: IntervalMap a
empty = E

fromList :: [(Word, Word, a)] -> IntervalMap a
fromList = foldl' (\m (a,b,v) -> insert a b v m) empty

toList :: IntervalMap a -> [(Word, Word, a)]
toList = flip go []
  where
    go  E                 =  id
    go (L a b v vs)       =  (:) (a,b,v) . (++) (map ((,,) a b) vs)
    go (N _ _ _ t1 t2)    =  go t1 . go t2
    go (M _ _ _ _ t1 t2)  =  go t1 . go t2

size :: IntervalMap a -> Int
size = go 0
  where
    go !l  E              = l
    go !l (L _ _ _    vs) = l + 1 + length vs
    go !l (N _ _ _   u v) = go (go l u) v
    go !l (M _ _ _ _ u v) = go (go l u) v

-- | Inserts a new interval into an 'IntervalMap'.  Intervals are
-- specified by starting and ending points and are understood as
-- half-open.  Intervals are  with zero length are legal, and the lookup
-- functions behave sensibly.

insert :: Key -> Key -> a -> IntervalMap a -> IntervalMap a
insert a b v E = L a b v []

insert a b v (L c d w ws)
    | a < c                    =  N (makePrefix a c) (max b d) (getMask a c) (L a b v []) (L c d w ws)
    | a > c                    =  N (makePrefix a c) (max b d) (getMask a c) (L c d w ws) (L a b v [])
    | b < d                    =  M (makePrefix b d) a (max b d) (getMask b d) (L a b v []) (L c d w ws)
    | b > d                    =  M (makePrefix b d) a (max b d) (getMask b d) (L c d w ws) (L a b v [])
    | otherwise                =  L a b v (w:ws)

insert a b v t@(N k h m t1 t2)
    | zero a k m && hasBit a m       =  N k (max h b) m t1 (insert a b v t2)
    | zero a k m                     =  N k (max h b) m (insert a b v t1) t2
    | a < k                          =  N (makePrefix a k) (max h b) (getMask a k) (L a b v []) t
    | otherwise                      =  N (makePrefix a k) (max h b) (getMask a k) t (L a b v [])

insert a b v t@(M k c h m t1 t2)
    | a < c                          =  N (makePrefix a c) (max b h) (getMask a c) (L a b v []) t
    | a > c                          =  N (makePrefix a c) (max b h) (getMask a c) t (L a b v [])
    | zero b k m && hasBit b m       =  M k c (max h b) m t1 (insert a b v t2)
    | zero b k m                     =  M k c (max h b) m (insert a b v t1) t2
    | b < k                          =  M (makePrefix b k) c (max h b) (getMask b k) (L a b v []) t
    | otherwise                      =  M (makePrefix b k) c (max h b) (getMask b k) t (L a b v [])


-- | Deletes up to one value that matches a predicate from those mapped to an exact range.
deleteBy :: (a -> Bool) -> Word -> Word -> IntervalMap a -> IntervalMap a
deleteBy p !a !b = go
  where
    go    E                                   =  E
    go t@(L c d _ _)      | a /= c || b /= d  =  t
    go t@(L _ _ v [    ]) | p v               =  E
                          | otherwise         =  t
    go   (L c d v (w:ws)) | p v               =  L c d w ws
                          | otherwise         =  L c d v (delete1 (w:ws))
    go t@(N k h m t1 t2)  | b > h             =  t
                          | a >= k            =  mkN k m t1 (go t2)
                          | otherwise         =  mkN k m (go t1) t2
    go t@(M k c h m t1 t2)| b > h             =  t
                          | b >= k            =  mkM k c m t1 (go t2)
                          | otherwise         =  mkM k c m (go t1) t2

    mkN _ _  E t2 = t2
    mkN _ _ t1  E = t1
    mkN k m t1 t2 = N k (getMax t1 `max` getMax t2) m t1 t2

    mkM _ _ _  E t2 = t2
    mkM _ _ _ t1  E = t1
    mkM k c m t1 t2 = M k c (getMax t1 `max` getMax t2) m t1 t2

    delete1 [    ]              =  []
    delete1 (x:xs) | p x        =  xs
                   | otherwise  =  x : delete1 xs

    getMax :: IntervalMap a -> Word
    getMax  E            = minBound
    getMax (L _ d   _ _) = d
    getMax (N _ h _ _ _) = h
    getMax (M _ _ h _ _ _) = h

-- | Given a 'Key' and a single 'Mask' bit, returns the common prefix
-- part of the key.
doMask :: Key -> Mask -> Prefix
doMask k m = k .&. complement (m .|. (m-1))

-- | Given two different keys, returns the 'Mask' in which on the
-- highest bit that differs between the keys is set.
getMask :: Key -> Key -> Mask
getMask a b = highest_bit `shiftR` countLeadingZeros (xor a b)

-- | Constructs the common prefix of two different keys.  The prefix
-- contains the high bits that match in both keys as they are, and the
-- highest bit that differs between the keys is set.  This implies that
-- @a < makePrefix a b <= b@.
--
makePrefix :: Key -> Key -> Prefix
makePrefix a b = let m = getMask a b in b .&. complement (m-1) .|. m

-- | Returns whether the part of two keys to the left of a mask bit are
-- the same.
zero :: Key -> Key -> Mask -> Bool
zero a b m = doMask (xor a b) m == 0

hasBit :: Key -> Mask -> Bool
hasBit a m = (a .&. m) /= 0

lookup :: Word -> Word -> IntervalMap a -> [a]
lookup = lookupWith (\_ _ -> id)

lookupWithKeys :: Word -> Word -> IntervalMap a -> [(Word,Word,a)]
lookupWithKeys = lookupWith (,,)

lookupWith :: (Word -> Word -> a -> b) -> Word -> Word -> IntervalMap a -> [b]
lookupWith f !a !b t = go t []
  where
    go  E                                  =  id
    go (L c d v vs)    | a == c && b == d  =  (:) (f c d v) . (++) (map (f c d) vs)
                       | otherwise         =  id
    go (N k h _ t1 t2) | b > h             =  id
                       | a < k             =  go t1
                       | otherwise         =  go t2
    go (M k _ h _ t1 t2) | b > h           =  id
                         | b >= k          =  go t2
                         | otherwise       =  go t1


-- | Returns all entries that overlap the query.  An empty interval is
-- considered to overlap only if it is strictly contained in the query.
lookupOverlap :: Word -> Word -> IntervalMap a -> [a]
lookupOverlap = lookupOverlapWith (\_ _ -> id)

lookupOverlapWithKeys :: Word -> Word -> IntervalMap a -> [(Word,Word,a)]
lookupOverlapWithKeys = lookupOverlapWith (,,)

lookupOverlapWith :: (Word -> Word -> a -> b) -> Word -> Word -> IntervalMap a -> [b]
lookupOverlapWith f !a !b t = go t []
  where
    go  E                                =  id
    go (L c d v vs)    | c < b && a < d  =  (:) (f c d v) . (++) (map (f c d) vs)
                       | otherwise       =  id
    go (N k h _ t1 t2) | a >= h          =  id
                       | b <= k          =  go t1                  -- this is correct because the split key is also the minimum
                       | otherwise       =  go t1 . go t2          -- always go left, because t1 might have a huge maximum
    go (M k _ h _ t1 t2) | a >= h          =  id
                         | a >= k          =  go t2
                         | otherwise       =  go t1 . go t2

-- | Returns all entries contained in the query.  Empty intervals must be
-- strictly contained.
lookupContained :: Word -> Word -> IntervalMap a -> [a]
lookupContained = lookupContainedWith (\_ _ -> id)

lookupContainedWithKeys :: Word -> Word -> IntervalMap a -> [(Word,Word,a)]
lookupContainedWithKeys = lookupContainedWith (,,)

lookupContainedWith :: (Word -> Word -> a -> b) -> Word -> Word -> IntervalMap a -> [b]
lookupContainedWith f !a !b t = go t []
  where
    go  E                                  =  id
    go (L c d v vs)    | a <= c && d <= b  =  (:) (f c d v) . (++) (map (f c d) vs)
                       | otherwise         =  id
    go (N k h _ t1 t2) | a >= h            =  id
                       | b <= k            =  go t1
                       | a >= k            =  go t2
                       | otherwise         =  go t1 . go t2
    go (M k _ h _ t1 t2) | a >= h            =  id
                         | b <  k            =  go t1
                         | a >= k            =  go t2
                         | otherwise         =  go t1 . go t2


-- | Returns all entries that contain in the query.  Empty intervals
-- don't contain anything.
lookupContaining :: Word -> Word -> IntervalMap a -> [a]
lookupContaining = lookupContainingWith (\_ _ -> id)

lookupContainingWithKeys :: Word -> Word -> IntervalMap a -> [(Word,Word,a)]
lookupContainingWithKeys = lookupContainingWith (,,)

lookupContainingWith :: (Word -> Word -> a -> b) -> Word -> Word -> IntervalMap a -> [b]
lookupContainingWith f !a !b t = go t []
  where
    go  E                                  =  id
    go (L c d v vs)    | c <= a && b <= d  =  (:) (f c d v) . (++) (map (f c d) vs)
                       | otherwise         =  id
    go (N k h _ t1 t2) | b > h             =  id
                       | a < k             =  go t1
                       | otherwise         =  go t1 . go t2
    go (M _ _ h _ t1 t2) | b > h             =  id
                         | otherwise         =  go t1 . go t2

-- | Returns entries that overlap the query but are not contained in it.
lookupPartial :: Word -> Word -> IntervalMap a -> [a]
lookupPartial = lookupPartialWith (\_ _ -> id)

lookupPartialWithKeys :: Word -> Word -> IntervalMap a -> [(Word,Word,a)]
lookupPartialWithKeys = lookupPartialWith (,,)

lookupPartialWith :: (Word -> Word -> a -> b) -> Word -> Word -> IntervalMap a -> [b]
lookupPartialWith f !a !b t = go t []
  where
    go  E                                                    =  id
    go (L c d v vs)    | c < b && a < d && (a > c || b < d)  =  (:) (f c d v) . (++) (map (f c d) vs)
                       | otherwise                           =  id
    go (N k h _ t1 t2) | a >= h                              =  id
                       | b <= k                              =  go t1
                       | otherwise                           =  go t1 . go t2
    go (M k _ h _ t1 t2) | a >= h          =  id
                         | a >= k          =  go t2
                         | otherwise       =  go t1 . go t2


-- | Returns the closest entry to the left of a query point.  Eligible
-- entries end at or before the query.  The closest entry is the one
-- starting last out of those eligible and ending last.
lookupLeft :: Word -> IntervalMap a -> Maybe ( Word, Word, [a] )
lookupLeft !a = go Nothing
  where
    go r  E                        =  r
    go r (L c d v vs) | d <= a     =  rightmost r (c,d,v:vs)
                      | otherwise  =  r

    -- If we already have a result and h is too small, we're done.
    go r@(Just (_,d,_)) (N _ h _ _ _) | h < d  =  r
    go r@(Just (_,d,_)) (M _ _ h _ _ _) | h < d  =  r

    -- Else If k <= a, search the right tree; then search left.
    go r (N k _ _ t1 t2)  =  go (if k <= a then go r t2 else r) t1
    go r (M _ _ _ _ t1 t2)  =  go (go r t2) t1

rightmost :: Maybe (Word,Word,a) -> (Word,Word,a) -> Maybe (Word,Word,a)
rightmost     Nothing        u          =  Just u
rightmost  v@(Just (e,f,_))  u@(c,d,_)  =  if (d,c) <= (f,e) then v else Just u


-- | Returns the closest entry to the right of a query point.  Eligible
-- entries start at or after the query.  The closest entry is the one
-- ending first out of those eligible and starting first.
lookupRight :: Word -> IntervalMap a -> Maybe ( Word, Word, [a])
lookupRight !b = go Nothing
  where
    go r  E                              =  r
    go r (L c d v vs) | c >= b           =  leftmost r (c,d,v:vs)
                      | otherwise        =  r

    go r (N k h _ t1 t2) | h < b      =  r
                         | k <= b     =  r'
                         | otherwise  =  go r' t1
      where
        r' = case r of Just (c,_,_) | k > c -> r
                       _                    -> go r t2

    go r (M k _ h _ t1 t2) | h < b      =  r
                           | k <= b     =  r'
                           | otherwise  =  go r' t1
      where
        r' = case r of Just (c,_,_) | k > c -> r
                       _                    -> go r t2

leftmost :: Maybe (Word,Word,a) -> (Word,Word,a) -> Maybe (Word,Word,a)
leftmost     Nothing        u          =  Just u
leftmost  v@(Just (e,f,_))  u@(c,d,_)  =  if (e,f) <= (c,d) then v else Just u

