{-# LANGUAGE CPP, LambdaCase, MultiWayIf, Rank2Types, TypeFamilies #-}

-- Simple, non-backtracking, no-lookahead LL parser combinators.
-- Modernized from code "(C) 2000-2002 Joe English.  Freely redistributable."

module Text.LLParser
    ( pTest, pCheck, pSym
    , pAnyByte, pTestByte, pTestChar8, pByte, pChar8
    , pEndOfString, pSpace, pSpan, pSpan1, pStringLit
    , pLiteral, pInt, pInteger, pHexInt, pFractional
    , pFollowedBy, pNotFollowedBy
    , isSpace_w8, isDigit_w8, isAlpha_w8, isHexDigit_w8
    , module Control.Applicative
    , module Control.Monad
    , pFoldr, pFoldr1, pFoldl, pFoldl1, pChainr, pChainl, pTry
    , pRun, Parser, (<?>)
    ) where

import Control.Applicative
import Control.Monad
#if !MIN_VERSION_base(4,13,0)
import Control.Monad.Fail
#endif
import Data.Char                ( chr )
import Data.String              ( IsString(..) )
import Data.Word                ( Word8 )
import Prelude

import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as C

-- generic stuff

infixl 4 <?>

newtype Parser str res = P { runP :: forall a .
       (res -> str -> a)                    -- ok continuation
    -> ([String] -> str -> a)               -- failure continuation
    -> ([String] -> str -> a)               -- error continuation
    -> (str -> a) }                         -- input, result

pTry :: Parser a b -> Parser a b
pTry (P pa) = P (\ok f _e i -> pa ok f (\ss _i' -> f ss i) i)

instance Functor (Parser a) where
    fmap f (P pb) = P (\ok -> pb (ok . f))

instance Alternative (Parser a) where
    empty = P (\_ok f _e -> f [])
    P pa <|> P pb = P (\ok f e -> pa ok (\ss -> pb ok (f . (++) ss) (e . (++) ss)) e)

instance Applicative (Parser a) where
    pure a = P (\ok _f _e  -> ok a)
    P pa <*> P pb = P (\ok f e -> pa (\a -> pb (ok . a) e e) f e)

instance MonadPlus (Parser a) where
    mzero = empty
    mplus = (<|>)

instance Monad (Parser a) where
    return = pure
    P pa >>= k = P (\ok f e -> pa (\a -> runP (k a) ok e e) f e)

instance MonadFail (Parser a) where
    fail m = P (\_ok f _e -> f [m])

instance (a ~ B.ByteString, b ~ ()) => IsString (Parser a b) where
    fromString = pLiteral . fromString

(<?>) :: Parser a b -> String -> Parser a b                      -- attach description
P pa <?> l = P (\ok f e -> pa ok (f . const [l]) (e . const [l]))    -- XXX

pFoldr :: (a->b->b) -> b -> Parser s a -> Parser s b
pFoldr op e p = loop where loop = (op <$> p <*> loop) <|> pure e

pFoldr1 :: (a->a->a) -> Parser s a -> Parser s a
pFoldr1 op p = go where go = do a <- p ; op a <$> go <|> pure a

pFoldl :: (b->a->b) -> b -> Parser s a -> Parser s b
pFoldl op e p = go e
  where
    go !a = (op a <$> p >>= go) <|> pure a

pFoldl1 :: (a->a->a) -> Parser s a -> Parser s a
pFoldl1 op p = p >>= go
  where
    go !a = (op a <$> p >>= go) <|> pure a

pChainr :: Parser a (b -> b -> b) -> Parser a b -> Parser a b
pChainr op p = loop where loop = p <**> ((flip <$> op <*> loop) <|> pure id)

pChainl :: Parser a (b -> b -> b) -> Parser a b -> Parser a b
pChainl op p = p >>= go
  where
    go !a = (op <*> pure a <*> p >>= go)
            <|> pure a

pRun :: Parser a b -> a -> Either ([String], a) (b,a)
pRun (P p) = p just2 none none where
    just2 x y       = Right (x,y)
    none x y        = Left  (x,y)

-- list stuff

pTest :: (a -> Bool) -> Parser [a] a
pTest p = P $ \ok f _e -> \case
    (c:cs) | p c -> ok c cs
    i            -> f [] i

pSym :: (Eq a, Show a) => a -> Parser [a] a
pSym a = pTest (a==) <?> show a

pCheck :: (a -> Maybe b) -> Parser [a] b
pCheck cmf = P (pcheck cmf)
  where
    pcheck _mf _ok f _e [] = f [] []
    pcheck  mf  ok f _e cs@(c:s) = case mf c of
        Just x  -> ok x s
        Nothing -> f [] cs

-- bytestring stuff

pAnyByte :: Parser B.ByteString Word8
pAnyByte = P $ \ok f _e i ->
    if | B.null i  -> f ["any byte"] i
       | otherwise -> ok (B.head i) (B.tail i)

pEndOfString :: Parser B.ByteString ()
pEndOfString = P $ \ok f _e i -> if B.null i then ok () i else f ["end of string"] i

pTestByte :: (Word8 -> Bool) -> Parser B.ByteString Word8
pTestByte p = P $ \ok f _e i ->
     if | B.null i     -> f [] i
        | p (B.head i) -> ok (B.head i) (B.tail i)
        | otherwise    -> f [] i

pByte :: Word8 -> Parser B.ByteString Word8
pByte b = pTestByte (b ==) <?> "0x" ++ show b

pFollowedBy :: (Word8 -> Bool) -> Parser B.ByteString ()
pFollowedBy p = P $ \ok f _e i ->
     if | B.null i     -> f  [] i
        | p (B.head i) -> ok () i
        | otherwise    -> f  [] i

pNotFollowedBy :: (Word8 -> Bool) -> Parser B.ByteString ()
pNotFollowedBy p = P $ \ok f _e i ->
     if | B.null i     -> ok () i
        | p (B.head i) -> f  [] i
        | otherwise    -> ok () i


pTestChar8 :: (Char -> Bool) -> Parser B.ByteString Char
pTestChar8 p = P $ \ok f _e i ->
     if | C.null i     -> f [] i
        | p (C.head i) -> ok (C.head i) (C.tail i)
        | otherwise    -> f [] i

pChar8 :: Char -> Parser B.ByteString Char
pChar8 c = pTestChar8 (c ==) <?> show c

pSpan :: (Word8 -> Bool) -> Parser B.ByteString B.ByteString
pSpan p = P $ \ok _f _e -> uncurry ok . B.span p

pSpan1 :: (Word8 -> Bool) -> Parser B.ByteString B.ByteString
pSpan1 p = P $ \ok f _e i -> case B.span p i of
    (a,b) -> if B.null a then f [] i else ok a b

pLiteral :: B.ByteString -> Parser B.ByteString ()
pLiteral w = P $ \ok f _e i -> maybe (f [show w] i) (ok ()) $ B.stripPrefix w i

pStringLit :: Parser B.ByteString String
pStringLit = pChar8 '"' *> many qchar <* pChar8 '"' <* pSpace
  where
    qchar = pTestChar8 (\c -> c /= '"' && c /= '\\') <|>
            pChar8 '\\' *> (pChar8 '\\' <|> pChar8 '"' <|> ucs)
    ucs = do cp <- pChar8 'u' *> pHexInt
             guard $ cp <= 0x10ffff
             return $ chr cp
          <?> "unicode escape"

pInt :: Parser B.ByteString Int
pInt = P $ \ok f _e i -> maybe (f ["integer"] i) (uncurry ok) $ C.readInt i

pInteger :: Parser B.ByteString Integer
pInteger = P $ \ok f _e i -> maybe (f ["integer"] i) (uncurry ok) $ C.readInteger i

pHexInt :: Num a => Parser B.ByteString a
pHexInt = pFoldl1 (\a d -> 16 * a + d) pHexDigit <?> "hex integer"

pHexDigit :: Num a => Parser B.ByteString a
pHexDigit = P $ \ok f _e i ->
    case B.head i of
        d | B.null i            ->  f [] i
          | 48 <= d && d <  58  ->  k 48
          | 64 <  d && d <  11  ->  k 55
          | 96 <  d && d < 103  ->  k 87
          | otherwise           ->  f [] i
          where
            k o  =  ok (fromIntegral $ d - o) (B.tail i)

pSpace :: Parser B.ByteString ()
pSpace = P $ \ok _f _e -> ok () . B.dropWhile isSpace_w8

isSpace_w8 :: Word8 -> Bool
isSpace_w8 w = w == 32 || w == 9 || w == 10 || w == 13 || w == 12 -- SPC, TAB, LF, CR, FF

isDigit_w8 :: Word8 -> Bool
isDigit_w8 w = 48 <= w && w < 58

isHexDigit_w8 :: Word8 -> Bool
isHexDigit_w8 w = 48 <= w && w < 58 || 64 < w && w < 71 || 96 < w && w < 103

isAlpha_w8 :: Word8 -> Bool
isAlpha_w8 w = 64 < w && w < 90 || 96 < w && w < 122

{-# SPECIALIZE pFractional ::
    Parser B.ByteString Float,
    Parser B.ByteString Double,
    Parser B.ByteString Rational #-}
pFractional :: Fractional a => Parser B.ByteString a
pFractional = conv
    <$> (pInteger <|> pure 0)
    <*> (pChar8 '.' *> pSpan1 isDigit_w8 <|> pure B.empty)
    <*> (pTry ((pChar8 'e' <|> pChar8 'E') *> pInt) <|> pure 0)
    <?> "real number"
  where
    conv whole part expot = mantissa * 10 ^^ expot
      where
        mantissa | whole >= 0 = fromInteger whole + fromInteger part' * base
                 | otherwise  = fromInteger whole - fromInteger part' * base
        part'                 = B.foldl' (\a c -> 10 * a + fromIntegral c - 48) 0 part
        base                  = 10 ^ B.length part


